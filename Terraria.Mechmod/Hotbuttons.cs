﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.Utilities;
using Terraria.ID;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Terraria.IO;

namespace Terraria.Mechmod
{
	public class HBContainer
	{
		public Action action;
		public Func<bool> function;
		public HotbarTexture texture;
		public bool implemented;
		public string name;
	}
	public static class Hotbuttons
	{

		//I made a script for this.

		public static Dictionary<string, HBContainer> buttons = new Dictionary<string, HBContainer>();

		public static Dictionary<int, Dictionary<int, string>> buttonMap = new Dictionary<int, Dictionary<int, string>>()
		{
			//This is a really silly way of doing it. I originally had other ideas, but oh well.
			//As we add buttons, uncomment the label here and change it to the button

			//Cheats
			[0] = new Dictionary<int, string>()
			{
				//[0] = "Infinite wire enabled",
				//[1] = "Infinite actuators enabled",
				[2] = "Godmode",
				//[3] = "Infinite health enabled",
				//[4] = "Infinite mana enabled",
				//[5] = "No placing limits",
				//[6] = "UFO speed:",
				//[7] = "Reset UFO speed",
			},
			//Wiring
			[1] = new Dictionary<int, string>()
			{
				//[0] =      "Red wire unaffected by light",
				//[1] =      "Blue wire unaffected by light",
				//[2] =      "Green wire unaffected by light",
				//[3] =      "Actuators unaffected by light",

				[4] =  "Wire opacity",
				[6] =      "(None)",//r        (None) means that when shift is held, it'll dim regardless of the fact that it doesn't have a button (its button is handled by Wire Opacity)
				[7] =      "(None)",//b			to be honest, it's more heavy .Contains() checks to actually do this, so currently itll work with any old thing 
				[8] =      "(None)",//g
				[5] =      "(None)",//a
				//[9] =      "Always show wires",
				//[10] =     "Wrenches highlight wire",
			},
			//Ghosts
			[2] = new Dictionary<int, string>()
			{
				[0] =  "Dummy Ghost Toggle",
				//[1] =  "Ghosts are unaffected by light",
				//[2] =  "Dummy ghosts outlined in red",
				//[3] =  "Dummies spawn when activated",
				//[4] =  "Kill wayward ghosts",
				//[5] =  "Brightness",
				//[6] =  "Show dummy IDs",
				//[7] =  "Show ghost feet",
			},
			//Hotbar
			[3] = new Dictionary<int, string>()
			{
				//[0] =      "Hotkeys",
				//[1] =      "Misc toggles",
				//[2] =      "Clear Hotbar",
			},
			//Other
			[4] = new Dictionary<int, string>()
			{
				//[0] =      "Reset all Mechmod settings",
				//[1] =      "Inventory button enabled",
				//[2] =      "Color: ",
				//[3] =      "Hotbar Color: ",
				//[4] =      "No clouds",
				//[5] =      "Disable easter egg",
			},
		};
		public static Dictionary<int, Dictionary<int, string>> altLabels = new Dictionary<int, Dictionary<int, string>>()
		{
			[0] = new Dictionary<int, string>()
			{
				[0] = "Infinite wire disabled",
				[1] = "Infinite actuators disabled",
				[2] = "Godmode disabled",
				[3] = "Infinite health disabled",
				[4] = "Infinite mana disabled",
				[5] = "Normal placing limits",
				[6] = "",
				[7] = "",
			},
			[1] = new Dictionary<int, string>()
			{
				[0] = "Red wire affected by light",
				[1] = "Blue wire affected by light",
				[2] = "Green wire affected by light",
				[3] = "Actuators affected by light",
				[4] = "",
				[5] = "",
				[6] = "",
				[7] = "",
				[8] = "",
				[9] = "Conditionally show wires",
				[10] = "Wrenches don't highlight wire",
			},
			[2] = new Dictionary<int, string>()
			{
				[0] = "",
				[1] = "Ghosts are affected by light",
				[2] = "Dummy ghosts not outlined",
				[3] = "Dummies spawn on world load",
				[4] = "",
				[5] = "",
				[6] = "Hide dummy IDs",
				[7] = "Hide ghost feet",
			},
			[3] = new Dictionary<int, string>()
			{
				[0] = "",
				[1] = "",
				[2] = "",
			},
			[4] = new Dictionary<int, string>()
			{
				[0] = "",
				[1] = "Inventory button hidden",
				[2] = "",
				[3] = "",
				[4] = "Clouds",
				[5] = "",
				[6] = "",
			},

		};
		public static void LoadDictionary()
		{
			AddButton("Godmode",
				() => Main.godmode = !Main.godmode,
				() => Main.godmode,
				new HotbarTexture(0, 0, 40, 40));
			AddButton("Daytime",
				() => Main.ToggleTime(false),
				new HotbarTexture(41, 0, 40, 40));
			AddButton("Nighttime",
				() => Main.ToggleTime(true),
				new HotbarTexture(82, 0, 40, 40));
			AddButton("Wire Opacity",
				MechmodOptions.Submenu.WIREOPACITY,
                new HotbarTexture(123, 0, 40, 40));
			AddButton("Screen Lock",
				() => { Main.screenLocked = !Main.screenLocked; Main.lockPosition = Main.screenPosition; },
				() => Main.screenLocked,
				new HotbarTexture(164, 0, 40, 40));

			AddButton("Red Rocket",
				() => Main.projectile[Projectile.NewProjectile(Main.player[Main.myPlayer].position.X, Main.player[Main.myPlayer].position.Y, 0f, -2f, ProjectileID.RocketFireworkRed, 0, 0f)].Kill(),
				new HotbarTexture(Main.itemTexture[ItemID.RedRocket]));
			AddButton("Green Rocket",
				() => Main.projectile[Projectile.NewProjectile(Main.player[Main.myPlayer].position.X, Main.player[Main.myPlayer].position.Y, 0f, -2f, ProjectileID.RocketFireworkGreen, 0, 0f)].Kill(),
				new HotbarTexture(Main.itemTexture[ItemID.YellowRocket]));
			AddButton("Dummy Ghost Toggle",
				() => Main.showDummyGhosts = !Main.showDummyGhosts,
				() => Main.showDummyGhosts,
				new HotbarTexture(Main.dummyGhostTexture, new Rectangle(34, 0, 32, 48)));

			AddButton("Save world",
				() => { Console.WriteLine("Saving world ..."); WorldFile.saveWorld(); Console.WriteLine("Sucess!"); },
				new HotbarTexture(246, 0, 40, 40));
			AddButton("Load world",
				() => { WorldFile.loadWorld(WorldFile.IsWorldOnCloud, true); if (Main.mapEnabled)Main.Map.Load();	if (Main.netMode != 2)	Main.sectionManager.SetAllFramesLoaded(); },
				new HotbarTexture(205, 0, 40, 40));


			HotbarButton.LoadSaveString();
		}
		public static void AddButton(string key, Action action, Func<bool> func, HotbarTexture hbt)
		{
			HBContainer container = new HBContainer();
			container.action = action;
			container.function = func;
			container.texture = hbt;
			container.implemented = false;
			container.name = key;
			buttons[key] = container;
		}
		public static void AddButton(string key, Action action, HotbarTexture hbt)
		{
			AddButton(key, action, () => false, hbt);
		}
		public static void AddButton(string key, MechmodOptions.Submenu submenu, HotbarTexture hbt)
		{
			AddButton(key, () => HotbarButton.ToggleSubmenu(submenu), () => HotbarButton.SubmenuDown(submenu), hbt);
		}
	}
	
}
