using Microsoft.Xna.Framework;
using System;
using Terraria.Graphics.Capture;

namespace Terraria.Graphics.MapCapture
{
    public class MapCaptureSettings : CaptureSettings
    {
        new public Rectangle Area;
        new public bool UseScaling = true;
        new public string OutputName;
        public bool MapCaptureEntities = true;
        new public MapCaptureBiome Biome = MapCaptureBiome.Biomes[0];
        public bool MapCaptureMech;
        public bool MapCaptureBackground;
        public MapCaptureSettings()
        {
            DateTime dateTime = DateTime.Now.ToLocalTime();
            this.OutputName = string.Concat(new string[]
            {
                "MapCapture ",
                dateTime.Year.ToString("D4"),
                "-",
                dateTime.Month.ToString("D2"),
                "-",
                dateTime.Day.ToString("D2"),
                " ",
                dateTime.Hour.ToString("D2"),
                "_",
                dateTime.Minute.ToString("D2"),
                "_",
                dateTime.Second.ToString("D2")
            });
        }
    }
}
