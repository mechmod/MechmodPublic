using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Terraria.UI.Chat;
namespace Terraria.Graphics.MapCapture
{
    internal class MapCaptureInterface
    {
        private static int numberOfButtons = 3;
        public static class Settings
        {
            //public static bool PackImage = true;
            //public static bool IncludeEntities = true;
            public static bool Copy = false;
            public static bool Paste = false;
            public static bool PastePressed = false;
            public static bool TP = false;
            public static bool TPPressed = false;
            public static bool TransparentBackground = false;
            public static int BiomeChoice = 0;
            public static int ScreenAnchor = 0;
            public static Color MarkedAreaColor = new Color(0.8f, 0.8f, 0.8f, 0f) * 0.3f;

        }
        private abstract class MapCaptureInterfaceMode
        {
            public bool Selected;
            public abstract void Update();
            public abstract void Draw(SpriteBatch sb);
            public abstract void ToggleActive(bool tickedOn);
            public abstract bool UsingMap();
        }
        private class ModeEdgeSelection : MapCaptureInterface.MapCaptureInterfaceMode
        {
            public override void Update()
            {
                if (!this.Selected)
                {
                    return;
                }
                Vector2 mouse = new Vector2((float)Main.mouseX, (float)Main.mouseY);
                this.EdgePlacement(mouse);
            }
            public override void Draw(SpriteBatch sb)
            {
                if (!this.Selected)
                {
                    return;
                }
                this.DrawMarkedArea(sb);
                this.DrawCursors(sb);
            }
            public override void ToggleActive(bool tickedOn)
            {
            }
            public override bool UsingMap()
            {
                return true;
            }
            private void EdgePlacement(Vector2 mouse)
            {
                if (MapCaptureInterface.JustActivated)
                {
                    return;
                }
                Point point;
                if (!Main.mapFullscreen)
                {
                    if (Main.mouseLeft && !MapCaptureInterface.Settings.TP && !MapCaptureInterface.Settings.TPPressed && !MapCaptureInterface.Settings.PastePressed && !MapCaptureInterface.Settings.Paste)
                    {
                        MapCaptureInterface.EdgeAPinned = true;
                        MapCaptureInterface.EdgeA = Main.MouseWorld.ToTileCoordinates();
                    }
                    if (Main.mouseRight && !MapCaptureInterface.Settings.TP && !MapCaptureInterface.Settings.TPPressed && !MapCaptureInterface.Settings.PastePressed && !MapCaptureInterface.Settings.Paste)
                    {
                        MapCaptureInterface.EdgeBPinned = true;
                        MapCaptureInterface.EdgeB = Main.MouseWorld.ToTileCoordinates();
                    }
                }
                else if (MapCaptureInterface.GetMapCoords((int)mouse.X, (int)mouse.Y, 0, out point))
                {
                    if (Main.mouseLeft && !MapCaptureInterface.Settings.TP && !MapCaptureInterface.Settings.TPPressed && !MapCaptureInterface.Settings.PastePressed && !MapCaptureInterface.Settings.Paste)
                    {
                        MapCaptureInterface.EdgeAPinned = true;
                        MapCaptureInterface.EdgeA = point;
                    }
                    if (Main.mouseRight && !MapCaptureInterface.Settings.TP && !MapCaptureInterface.Settings.TPPressed && !MapCaptureInterface.Settings.PastePressed && !MapCaptureInterface.Settings.Paste)
                    {
                        MapCaptureInterface.EdgeBPinned = true;
                        MapCaptureInterface.EdgeB = point;
                    }
                }
                MapCaptureInterface.ConstraintPoints();
            }
            private void DrawMarkedArea(SpriteBatch sb)
            {
                if (!MapCaptureInterface.EdgeAPinned || !MapCaptureInterface.EdgeBPinned)
                {
                    return;
                }
                int num = Math.Min(MapCaptureInterface.EdgeA.X, MapCaptureInterface.EdgeB.X);
                int num2 = Math.Min(MapCaptureInterface.EdgeA.Y, MapCaptureInterface.EdgeB.Y);
                int num3 = Math.Abs(MapCaptureInterface.EdgeA.X - MapCaptureInterface.EdgeB.X);
                int num4 = Math.Abs(MapCaptureInterface.EdgeA.Y - MapCaptureInterface.EdgeB.Y);
                if (!Main.mapFullscreen)
                {
                    Rectangle rectangle = Main.ReverseGravitySupport(new Rectangle(num * 16, num2 * 16, (num3 + 1) * 16, (num4 + 1) * 16));
                    Rectangle rectangle2 = Main.ReverseGravitySupport(new Rectangle((int)Main.screenPosition.X, (int)Main.screenPosition.Y, Main.screenWidth + 1, Main.screenHeight + 1));
                    Rectangle destinationRectangle;
                    Rectangle.Intersect(ref rectangle2, ref rectangle, out destinationRectangle);
                    if (destinationRectangle.Width == 0 || destinationRectangle.Height == 0)
                    {
                        return;
                    }
                    destinationRectangle.Offset(-rectangle2.X, -rectangle2.Y);
                    sb.Draw(Main.magicPixel, destinationRectangle, MapCaptureInterface.Settings.MarkedAreaColor);
                    for (int i = 0; i < 2; i++)
                    {
                        sb.Draw(Main.magicPixel, new Rectangle(destinationRectangle.X, destinationRectangle.Y + ((i == 1) ? destinationRectangle.Height : -2), destinationRectangle.Width, 2), Color.White);
                        sb.Draw(Main.magicPixel, new Rectangle(destinationRectangle.X + ((i == 1) ? destinationRectangle.Width : -2), destinationRectangle.Y, 2, destinationRectangle.Height), Color.White);
                    }
                    return;
                }
                else
                {
                    Point point;
                    MapCaptureInterface.GetMapCoords(num, num2, 1, out point);
                    Point point2;
                    MapCaptureInterface.GetMapCoords(num + num3 + 1, num2 + num4 + 1, 1, out point2);
                    Rectangle rectangle3 = new Rectangle(point.X, point.Y, point2.X - point.X, point2.Y - point.Y);
                    Rectangle rectangle4 = new Rectangle(0, 0, Main.screenWidth + 1, Main.screenHeight + 1);
                    Rectangle destinationRectangle2;
                    Rectangle.Intersect(ref rectangle4, ref rectangle3, out destinationRectangle2);
                    if (destinationRectangle2.Width == 0 || destinationRectangle2.Height == 0)
                    {
                        return;
                    }
                    destinationRectangle2.Offset(-rectangle4.X, -rectangle4.Y);
                    sb.Draw(Main.magicPixel, destinationRectangle2, MapCaptureInterface.Settings.MarkedAreaColor);
                    for (int j = 0; j < 2; j++)
                    {
                        sb.Draw(Main.magicPixel, new Rectangle(destinationRectangle2.X, destinationRectangle2.Y + ((j == 1) ? destinationRectangle2.Height : -2), destinationRectangle2.Width, 2), Color.White);
                        sb.Draw(Main.magicPixel, new Rectangle(destinationRectangle2.X + ((j == 1) ? destinationRectangle2.Width : -2), destinationRectangle2.Y, 2, destinationRectangle2.Height), Color.White);
                    }
                    return;
                }
            }
            private void DrawCursors(SpriteBatch sb)
            {
                float num = 1f / Main.cursorScale;
                float num2 = 0.8f / num;
                Vector2 vector = Main.screenPosition + new Vector2(30f);
                Vector2 vector2 = vector + new Vector2((float)Main.screenWidth, (float)Main.screenHeight) - new Vector2(60f);
                if (Main.mapFullscreen)
                {
                    vector -= Main.screenPosition;
                    vector2 -= Main.screenPosition;
                }
                Vector3 vector3 = Main.rgbToHsl(Main.cursorColor);
                Color color = Main.hslToRgb((vector3.X + 0.33f) % 1f, vector3.Y, vector3.Z);
                Color color2 = Main.hslToRgb((vector3.X - 0.33f) % 1f, vector3.Y, vector3.Z);
                color2 = (color = Color.White);
                bool flag = Main.player[Main.myPlayer].gravDir == -1f;
                if (!MapCaptureInterface.EdgeAPinned)
                {
                    Utils.DrawCursorSingle(sb, color, 3.926991f, Main.cursorScale * num * num2, new Vector2((float)Main.mouseX - 5f + 12f, (float)Main.mouseY + 2.5f + 12f), 4, 0);
                }
                else
                {
                    int specialMode = 0;
                    float num3 = 0f;
                    Vector2 vector4 = Vector2.Zero;
                    if (!Main.mapFullscreen)
                    {
                        Vector2 vector5 = MapCaptureInterface.EdgeA.ToVector2() * 16f;
                        if (!MapCaptureInterface.EdgeBPinned)
                        {
                            specialMode = 1;
                            vector5 += Vector2.One * 8f;
                            num3 = (-vector5 + Main.ReverseGravitySupport(new Vector2((float)Main.mouseX, (float)Main.mouseY), 0f) + Main.screenPosition).ToRotation();
                            if (flag)
                            {
                                num3 = -num3;
                            }
                            vector4 = Vector2.Clamp(vector5, vector, vector2);
                            if (vector4 != vector5)
                            {
                                num3 = (vector5 - vector4).ToRotation();
                            }
                        }
                        else
                        {
                            Vector2 value = new Vector2((float)((MapCaptureInterface.EdgeA.X > MapCaptureInterface.EdgeB.X).ToInt() * 16), (float)((MapCaptureInterface.EdgeA.Y > MapCaptureInterface.EdgeB.Y).ToInt() * 16));
                            vector5 += value;
                            vector4 = Vector2.Clamp(vector5, vector, vector2);
                            num3 = (MapCaptureInterface.EdgeB.ToVector2() * 16f + new Vector2(16f) - value - vector4).ToRotation();
                            if (vector4 != vector5)
                            {
                                num3 = (vector5 - vector4).ToRotation();
                                specialMode = 1;
                            }
                            if (flag)
                            {
                                num3 *= -1f;
                            }
                        }
                        Utils.DrawCursorSingle(sb, color, num3 - 1.57079637f, Main.cursorScale * num, Main.ReverseGravitySupport(vector4 - Main.screenPosition, 0f), 4, specialMode);
                    }
                    else
                    {
                        Point edgeA = MapCaptureInterface.EdgeA;
                        if (MapCaptureInterface.EdgeBPinned)
                        {
                            int num4 = (MapCaptureInterface.EdgeA.X > MapCaptureInterface.EdgeB.X).ToInt();
                            int num5 = (MapCaptureInterface.EdgeA.Y > MapCaptureInterface.EdgeB.Y).ToInt();
                            edgeA.X += num4;
                            edgeA.Y += num5;
                            MapCaptureInterface.GetMapCoords(edgeA.X, edgeA.Y, 1, out edgeA);
                            Point edgeB = MapCaptureInterface.EdgeB;
                            edgeB.X += 1 - num4;
                            edgeB.Y += 1 - num5;
                            MapCaptureInterface.GetMapCoords(edgeB.X, edgeB.Y, 1, out edgeB);
                            vector4 = edgeA.ToVector2();
                            vector4 = Vector2.Clamp(vector4, vector, vector2);
                            num3 = (edgeB.ToVector2() - vector4).ToRotation();
                        }
                        else
                        {
                            MapCaptureInterface.GetMapCoords(edgeA.X, edgeA.Y, 1, out edgeA);
                        }
                        Utils.DrawCursorSingle(sb, color, num3 - 1.57079637f, Main.cursorScale * num, edgeA.ToVector2(), 4, 0);
                    }
                }
                if (!MapCaptureInterface.EdgeBPinned)
                {
                    Utils.DrawCursorSingle(sb, color2, 0.7853982f, Main.cursorScale * num * num2, new Vector2((float)Main.mouseX + 2.5f + 12f, (float)Main.mouseY - 5f + 12f), 5, 0);
                    return;
                }
                int specialMode2 = 0;
                float num6 = 0f;
                Vector2 vector6 = Vector2.Zero;
                if (!Main.mapFullscreen)
                {
                    Vector2 vector7 = MapCaptureInterface.EdgeB.ToVector2() * 16f;
                    if (!MapCaptureInterface.EdgeAPinned)
                    {
                        specialMode2 = 1;
                        vector7 += Vector2.One * 8f;
                        num6 = (-vector7 + Main.ReverseGravitySupport(new Vector2((float)Main.mouseX, (float)Main.mouseY), 0f) + Main.screenPosition).ToRotation();
                        if (flag)
                        {
                            num6 = -num6;
                        }
                        vector6 = Vector2.Clamp(vector7, vector, vector2);
                        if (vector6 != vector7)
                        {
                            num6 = (vector7 - vector6).ToRotation();
                        }
                    }
                    else
                    {
                        Vector2 value2 = new Vector2((float)((MapCaptureInterface.EdgeB.X >= MapCaptureInterface.EdgeA.X).ToInt() * 16), (float)((MapCaptureInterface.EdgeB.Y >= MapCaptureInterface.EdgeA.Y).ToInt() * 16));
                        vector7 += value2;
                        vector6 = Vector2.Clamp(vector7, vector, vector2);
                        num6 = (MapCaptureInterface.EdgeA.ToVector2() * 16f + new Vector2(16f) - value2 - vector6).ToRotation();
                        if (vector6 != vector7)
                        {
                            num6 = (vector7 - vector6).ToRotation();
                            specialMode2 = 1;
                        }
                        if (flag)
                        {
                            num6 *= -1f;
                        }
                    }
                    Utils.DrawCursorSingle(sb, color2, num6 - 1.57079637f, Main.cursorScale * num, Main.ReverseGravitySupport(vector6 - Main.screenPosition, 0f), 5, specialMode2);
                    return;
                }
                Point edgeB2 = MapCaptureInterface.EdgeB;
                if (MapCaptureInterface.EdgeAPinned)
                {
                    int num7 = (MapCaptureInterface.EdgeB.X >= MapCaptureInterface.EdgeA.X).ToInt();
                    int num8 = (MapCaptureInterface.EdgeB.Y >= MapCaptureInterface.EdgeA.Y).ToInt();
                    edgeB2.X += num7;
                    edgeB2.Y += num8;
                    MapCaptureInterface.GetMapCoords(edgeB2.X, edgeB2.Y, 1, out edgeB2);
                    Point edgeA2 = MapCaptureInterface.EdgeA;
                    edgeA2.X += 1 - num7;
                    edgeA2.Y += 1 - num8;
                    MapCaptureInterface.GetMapCoords(edgeA2.X, edgeA2.Y, 1, out edgeA2);
                    vector6 = edgeB2.ToVector2();
                    vector6 = Vector2.Clamp(vector6, vector, vector2);
                    num6 = (edgeA2.ToVector2() - vector6).ToRotation();
                }
                else
                {
                    MapCaptureInterface.GetMapCoords(edgeB2.X, edgeB2.Y, 1, out edgeB2);
                }
                Utils.DrawCursorSingle(sb, color2, num6 - 1.57079637f, Main.cursorScale * num, edgeB2.ToVector2(), 5, 0);
            }
        }
        private class ModeDragBounds : MapCaptureInterface.MapCaptureInterfaceMode
        {
            public int currentAim = -1;
            private bool dragging;
            private int caughtEdge = -1;
            private bool inMap;
            public override void Update()
            {
                if (!this.Selected)
                {
                    return;
                }
                if (MapCaptureInterface.JustActivated)
                {
                    return;
                }
                Vector2 mouse = new Vector2((float)Main.mouseX, (float)Main.mouseY);
                this.DragBounds(mouse);
            }
            public override void Draw(SpriteBatch sb)
            {
                if (!this.Selected)
                {
                    return;
                }
                this.DrawMarkedArea(sb);
            }
            public override void ToggleActive(bool tickedOn)
            {
                if (!tickedOn)
                {
                    this.currentAim = -1;
                }
            }
            public override bool UsingMap()
            {
                return this.caughtEdge != -1;
            }
            private void DragBounds(Vector2 mouse) //set bounds, the one where you drag the lines, not the corners
            {
                if (!MapCaptureInterface.EdgeAPinned || !MapCaptureInterface.EdgeBPinned)
                {
                    bool flag = false;
                    if (Main.mouseLeft && !MapCaptureInterface.Settings.TP)
                    {
                        flag = true;
                    }
                    if (flag)
                    {
                        bool flag2 = true;
                        Point point;
                        if (!Main.mapFullscreen)
                        {
                            point = (Main.screenPosition + mouse).ToTileCoordinates();
                        }
                        else
                        {
                            flag2 = MapCaptureInterface.GetMapCoords((int)mouse.X, (int)mouse.Y, 0, out point);
                        }
                        if (flag2)
                        {
                            if (!MapCaptureInterface.EdgeAPinned)
                            {
                                MapCaptureInterface.EdgeAPinned = true;
                                MapCaptureInterface.EdgeA = point;
                            }
                            if (!MapCaptureInterface.EdgeBPinned)
                            {
                                MapCaptureInterface.EdgeBPinned = true;
                                MapCaptureInterface.EdgeB = point;
                            }
                        }
                        this.currentAim = 3;
                        this.caughtEdge = 1;
                    }
                }
                int num = Math.Min(MapCaptureInterface.EdgeA.X, MapCaptureInterface.EdgeB.X);
                int num2 = Math.Min(MapCaptureInterface.EdgeA.Y, MapCaptureInterface.EdgeB.Y);
                int num3 = Math.Abs(MapCaptureInterface.EdgeA.X - MapCaptureInterface.EdgeB.X);
                int num4 = Math.Abs(MapCaptureInterface.EdgeA.Y - MapCaptureInterface.EdgeB.Y);
                bool value = Main.player[Main.myPlayer].gravDir == -1f;
                int num5 = 1 - value.ToInt();
                int num6 = value.ToInt();
                Rectangle rectangle;
                Rectangle rectangle2;
                if (!Main.mapFullscreen)
                {
                    rectangle = Main.ReverseGravitySupport(new Rectangle(num * 16, num2 * 16, (num3 + 1) * 16, (num4 + 1) * 16));
                    rectangle2 = Main.ReverseGravitySupport(new Rectangle((int)Main.screenPosition.X, (int)Main.screenPosition.Y, Main.screenWidth + 1, Main.screenHeight + 1));
                    Rectangle rectangle3;
                    Rectangle.Intersect(ref rectangle2, ref rectangle, out rectangle3);
                    if (rectangle3.Width == 0 || rectangle3.Height == 0)
                    {
                        return;
                    }
                    rectangle3.Offset(-rectangle2.X, -rectangle2.Y);
                }
                else
                {
                    Point point2;
                    MapCaptureInterface.GetMapCoords(num, num2, 1, out point2);
                    Point point3;
                    MapCaptureInterface.GetMapCoords(num + num3 + 1, num2 + num4 + 1, 1, out point3);
                    rectangle = new Rectangle(point2.X, point2.Y, point3.X - point2.X, point3.Y - point2.Y);
                    rectangle2 = new Rectangle(0, 0, Main.screenWidth + 1, Main.screenHeight + 1);
                    Rectangle rectangle3;
                    Rectangle.Intersect(ref rectangle2, ref rectangle, out rectangle3);
                    if (rectangle3.Width == 0 || rectangle3.Height == 0)
                    {
                        return;
                    }
                    rectangle3.Offset(-rectangle2.X, -rectangle2.Y);
                }
                this.dragging = false;
                if (!Main.mouseLeft)
                {
                    this.currentAim = -1;
                }
                if (this.currentAim != -1)
                {
                    this.dragging = true;
                    Point point4 = default(Point);
                    if (!Main.mapFullscreen)
                    {
                        point4 = Main.MouseWorld.ToTileCoordinates();
                    }
                    else
                    {
                        Point point5;
                        if (!MapCaptureInterface.GetMapCoords((int)mouse.X, (int)mouse.Y, 0, out point5))
                        {
                            return;
                        }
                        point4 = point5;
                    }
                    switch (this.currentAim)
                    {
                        case 0:
                        case 1:
                            if (this.caughtEdge == 0)
                            {
                                MapCaptureInterface.EdgeA.Y = point4.Y;
                            }
                            if (this.caughtEdge == 1)
                            {
                                MapCaptureInterface.EdgeB.Y = point4.Y;
                            }
                            break;
                        case 2:
                        case 3:
                            if (this.caughtEdge == 0)
                            {
                                MapCaptureInterface.EdgeA.X = point4.X;
                            }
                            if (this.caughtEdge == 1)
                            {
                                MapCaptureInterface.EdgeB.X = point4.X;
                            }
                            break;
                    }
                }
                else
                {
                    this.caughtEdge = -1;
                    Rectangle drawbox = rectangle;
                    drawbox.Offset(-rectangle2.X, -rectangle2.Y);
                    this.inMap = drawbox.Contains(mouse.ToPoint());
                    int i = 0;
                    while (i < 4)
                    {
                        Rectangle bound = this.GetBound(drawbox, i);
                        bound.Inflate(8, 8);
                        if (bound.Contains(mouse.ToPoint()))
                        {
                            this.currentAim = i;
                            if (i == 0)
                            {
                                if (MapCaptureInterface.EdgeA.Y < MapCaptureInterface.EdgeB.Y)
                                {
                                    this.caughtEdge = num6;
                                    break;
                                }
                                this.caughtEdge = num5;
                                break;
                            }
                            else if (i == 1)
                            {
                                if (MapCaptureInterface.EdgeA.Y >= MapCaptureInterface.EdgeB.Y)
                                {
                                    this.caughtEdge = num6;
                                    break;
                                }
                                this.caughtEdge = num5;
                                break;
                            }
                            else if (i == 2)
                            {
                                if (MapCaptureInterface.EdgeA.X < MapCaptureInterface.EdgeB.X)
                                {
                                    this.caughtEdge = 0;
                                    break;
                                }
                                this.caughtEdge = 1;
                                break;
                            }
                            else
                            {
                                if (i != 3)
                                {
                                    break;
                                }
                                if (MapCaptureInterface.EdgeA.X >= MapCaptureInterface.EdgeB.X)
                                {
                                    this.caughtEdge = 0;
                                    break;
                                }
                                this.caughtEdge = 1;
                                break;
                            }
                        }
                        else
                        {
                            i++;
                        }
                    }
                }
                MapCaptureInterface.ConstraintPoints();
            }
            private Rectangle GetBound(Rectangle drawbox, int boundIndex)
            {
                if (boundIndex == 0)
                {
                    return new Rectangle(drawbox.X, drawbox.Y - 2, drawbox.Width, 2);
                }
                if (boundIndex == 1)
                {
                    return new Rectangle(drawbox.X, drawbox.Y + drawbox.Height, drawbox.Width, 2);
                }
                if (boundIndex == 2)
                {
                    return new Rectangle(drawbox.X - 2, drawbox.Y, 2, drawbox.Height);
                }
                if (boundIndex == 3)
                {
                    return new Rectangle(drawbox.X + drawbox.Width, drawbox.Y, 2, drawbox.Height);
                }
                return Rectangle.Empty;
            }
            public void DrawMarkedArea(SpriteBatch sb)
            {
                if (!MapCaptureInterface.EdgeAPinned || !MapCaptureInterface.EdgeBPinned)
                {
                    return;
                }
                int num = Math.Min(MapCaptureInterface.EdgeA.X, MapCaptureInterface.EdgeB.X);
                int num2 = Math.Min(MapCaptureInterface.EdgeA.Y, MapCaptureInterface.EdgeB.Y);
                int num3 = Math.Abs(MapCaptureInterface.EdgeA.X - MapCaptureInterface.EdgeB.X);
                int num4 = Math.Abs(MapCaptureInterface.EdgeA.Y - MapCaptureInterface.EdgeB.Y);
                Rectangle destinationRectangle;
                if (!Main.mapFullscreen)
                {
                    Rectangle rectangle = Main.ReverseGravitySupport(new Rectangle(num * 16, num2 * 16, (num3 + 1) * 16, (num4 + 1) * 16));
                    Rectangle rectangle2 = Main.ReverseGravitySupport(new Rectangle((int)Main.screenPosition.X, (int)Main.screenPosition.Y, Main.screenWidth + 1, Main.screenHeight + 1));
                    Rectangle.Intersect(ref rectangle2, ref rectangle, out destinationRectangle);
                    if (destinationRectangle.Width == 0 || destinationRectangle.Height == 0)
                    {
                        return;
                    }
                    destinationRectangle.Offset(-rectangle2.X, -rectangle2.Y);
                }
                else
                {
                    Point point;
                    MapCaptureInterface.GetMapCoords(num, num2, 1, out point);
                    Point point2;
                    MapCaptureInterface.GetMapCoords(num + num3 + 1, num2 + num4 + 1, 1, out point2);
                    Rectangle rectangle = new Rectangle(point.X, point.Y, point2.X - point.X, point2.Y - point.Y);
                    Rectangle rectangle2 = new Rectangle(0, 0, Main.screenWidth + 1, Main.screenHeight + 1);
                    Rectangle.Intersect(ref rectangle2, ref rectangle, out destinationRectangle);
                    if (destinationRectangle.Width == 0 || destinationRectangle.Height == 0)
                    {
                        return;
                    }
                    destinationRectangle.Offset(-rectangle2.X, -rectangle2.Y);
                }
                sb.Draw(Main.magicPixel, destinationRectangle, MapCaptureInterface.Settings.MarkedAreaColor);
                Rectangle empty = Rectangle.Empty;
                for (int i = 0; i < 2; i++)
                {
                    if (this.currentAim != i)
                    {
                        this.DrawBound(sb, new Rectangle(destinationRectangle.X, destinationRectangle.Y + ((i == 1) ? destinationRectangle.Height : -2), destinationRectangle.Width, 2), 0);
                    }
                    else
                    {
                        empty = new Rectangle(destinationRectangle.X, destinationRectangle.Y + ((i == 1) ? destinationRectangle.Height : -2), destinationRectangle.Width, 2);
                    }
                    if (this.currentAim != i + 2)
                    {
                        this.DrawBound(sb, new Rectangle(destinationRectangle.X + ((i == 1) ? destinationRectangle.Width : -2), destinationRectangle.Y, 2, destinationRectangle.Height), 0);
                    }
                    else
                    {
                        empty = new Rectangle(destinationRectangle.X + ((i == 1) ? destinationRectangle.Width : -2), destinationRectangle.Y, 2, destinationRectangle.Height);
                    }
                }
                if (empty != Rectangle.Empty)
                {
                    this.DrawBound(sb, empty, 1 + this.dragging.ToInt());
                }
            }
            private void DrawBound(SpriteBatch sb, Rectangle r, int mode)
            {
                if (mode == 0)
                {
                    sb.Draw(Main.magicPixel, r, Color.Silver);
                    return;
                }
                if (mode == 1)
                {
                    Rectangle destinationRectangle = new Rectangle(r.X - 2, r.Y, r.Width + 4, r.Height);
                    sb.Draw(Main.magicPixel, destinationRectangle, Color.White);
                    destinationRectangle = new Rectangle(r.X, r.Y - 2, r.Width, r.Height + 4);
                    sb.Draw(Main.magicPixel, destinationRectangle, Color.White);
                    sb.Draw(Main.magicPixel, r, Color.White);
                    return;
                }
                if (mode == 2)
                {
                    Rectangle destinationRectangle2 = new Rectangle(r.X - 2, r.Y, r.Width + 4, r.Height);
                    sb.Draw(Main.magicPixel, destinationRectangle2, Color.Gold);
                    destinationRectangle2 = new Rectangle(r.X, r.Y - 2, r.Width, r.Height + 4);
                    sb.Draw(Main.magicPixel, destinationRectangle2, Color.Gold);
                    sb.Draw(Main.magicPixel, r, Color.Gold);
                }
            }
        }
        private class ModeChangeSettings : MapCaptureInterface.MapCaptureInterfaceMode
        {
            private int hoveredButton = -1;
            private bool inUI;
            private Rectangle GetRect()
            {
                Rectangle result = new Rectangle(0, 0, 224, 170);
                int screenAnchor = MapCaptureInterface.Settings.ScreenAnchor;
                if (screenAnchor == 0)
                {
                    result.X = 227 - result.Width / 2;
                    result.Y = 80;
                    int num = 0;
                    Player player = Main.player[Main.myPlayer];
                    while (num < player.buffTime.Length && player.buffTime[num] > 0)
                    {
                        num++;
                    }
                    int num2 = num / 11;
                    num2 += ((num % 11 >= 3) ? 1 : 0);
                    result.Y += 48 * num2;
                }
                return result;
            }
            private void ButtonDraw(int button, ref string key, ref string value)
            {
                switch (button) //numberOfButtons
                {
                    case 0:
                        key = "Copy";
                        value = Lang.inter[73 - MapCaptureInterface.Settings.Copy.ToInt()]; //On/off
                        return;
                    case 1:
                        key = "Paste";
                        value = Lang.inter[73 - MapCaptureInterface.Settings.Paste.ToInt()];
                        return;
                    case 2:
                        key = "Teleport";
                        value = Lang.inter[73 - (!MapCaptureInterface.Settings.TP).ToInt()];
                        return;/*
                    case 3:
                    case 4:
                    case 5:
                        break;
                    case 6:
                        key = "      " + Lang.menu[86];
                        value = "";
                        break;*/
                    default:
                        return;
                }
            }
            private void PressButton(int button)
            {
                switch (button) //numberOfButtons
                {
                    case 0:
                        MapCaptureInterface.Settings.Copy = !MapCaptureInterface.Settings.Copy;
                        return;
                    case 1:
                        /*MapCaptureInterface.Settings.Paste = !MapCaptureInterface.Settings.Paste;*/
                        return;
                    case 2: /*MapCaptureInterface.Settings.TP = !MapCaptureInterface.Settings.TP;*/ return;
                    /*case 2:
                        MapCaptureInterface.Settings.TransparentBackground = !MapCaptureInterface.Settings.TransparentBackground;
                        return;
                    case 3:
                    case 4:
                    case 5:
                        break;
                    case 6:
                        MapCaptureInterface.Settings.PackImage = false;
                        MapCaptureInterface.Settings.IncludeEntities = true;
                        MapCaptureInterface.Settings.TransparentBackground = false;
                        MapCaptureInterface.Settings.BiomeChoice = 0;
                        break;*/
                    default:
                        return;
                }
            }
            private void DrawWaterChoices(SpriteBatch spritebatch, Point start, Point mouse)
            {
                Rectangle r = new Rectangle(0, 0, 20, 20);
                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (i != 1 || j != 3)
                        {
                            int index = j + i * 5;
                            r.X = start.X + 24 * j + 12 * i;
                            r.Y = start.Y + 24 * i;
                            if (i == 1 && j == 4)
                            {
                                r.X -= 24;
                            }
                            int num = 0;
                            if (r.Contains(mouse))
                            {
                                if (Main.mouseLeft && Main.mouseLeftRelease)
                                {
                                    MapCaptureInterface.Settings.BiomeChoice = this.BiomeWater(index);
                                }
                                num++;
                            }
                            if (MapCaptureInterface.Settings.BiomeChoice == this.BiomeWater(index))
                            {
                                num += 2;
                            }
                            Texture2D texture = Main.liquidTexture[this.BiomeWater(index)];
                            int x = (int)Main.wFrame * 18;
                            Color arg_CF_0 = Color.White;
                            float num2 = 1f;
                            if (num < 2)
                            {
                                num2 *= 0.5f;
                            }
                            if (num % 2 == 1)
                            {
                                spritebatch.Draw(Main.magicPixel, r.TopLeft(), new Rectangle?(new Rectangle(0, 0, 1, 1)), Color.Gold, 0f, Vector2.Zero, new Vector2(20f), SpriteEffects.None, 0f);
                            }
                            else
                            {
                                spritebatch.Draw(Main.magicPixel, r.TopLeft(), new Rectangle?(new Rectangle(0, 0, 1, 1)), Color.White * num2, 0f, Vector2.Zero, new Vector2(20f), SpriteEffects.None, 0f);
                            }
                            spritebatch.Draw(texture, r.TopLeft() + new Vector2(2f), new Rectangle?(new Rectangle(x, 0, 16, 16)), Color.White * num2);
                        }
                    }
                }
            }
            private int BiomeWater(int index)
            {
                switch (index)
                {
                    case 0:
                        return 0;
                    case 1:
                        return 2;
                    case 2:
                        return 3;
                    case 3:
                        return 4;
                    case 4:
                        return 5;
                    case 5:
                        return 6;
                    case 6:
                        return 7;
                    case 7:
                        return 8;
                    case 8:
                        return 9;
                    case 9:
                        return 10;
                    default:
                        return 0;
                }
            }
            public override void Update()
            {
                if (!this.Selected)
                {
                    return;
                }
                if (MapCaptureInterface.JustActivated)
                {
                    return;
                }
                Point value = new Point(Main.mouseX, Main.mouseY);
                this.hoveredButton = -1;
                Rectangle rect = this.GetRect();
                this.inUI = rect.Contains(value);
                rect.Inflate(-20, -20);
                rect.Height = 16;
                int y = rect.Y;
                for (int i = 0; i < 3; i++)
                {
                    rect.Y = y + i * 20;
                    if (rect.Contains(value))
                    {
                        this.hoveredButton = i;
                        break;
                    }
                }
                if (Main.mouseLeft && Main.mouseLeftRelease && this.hoveredButton != -1)
                {
                    this.PressButton(this.hoveredButton);
                }
            }
            public override void Draw(SpriteBatch sb)
            {
                if (!this.Selected)
                {
                    return;
                }
                ((MapCaptureInterface.ModeDragBounds)MapCaptureInterface.Modes[1]).currentAim = -1;
                ((MapCaptureInterface.ModeDragBounds)MapCaptureInterface.Modes[1]).DrawMarkedArea(sb);
                Rectangle rect = this.GetRect();
                Utils.DrawInvBG(sb, rect, new Color(63, 65, 151, 255) * 0.485f);
                for (int i = 0; i < numberOfButtons; i++) //Number of buttons
                {
                    string text = "";
                    string text2 = "";
                    this.ButtonDraw(i, ref text, ref text2);
                    Color baseColor = Color.White;
                    if (i == this.hoveredButton)
                    {
                        baseColor = Color.Gold;
                    }
                    ChatManager.DrawColorCodedStringWithShadow(sb, Main.fontItemStack, text, rect.TopLeft() + new Vector2(20f, (float)(20 + 20 * i)), baseColor, 0f, Vector2.Zero, Vector2.One, -1f, 2f);
                    ChatManager.DrawColorCodedStringWithShadow(sb, Main.fontItemStack, text2, rect.TopRight() + new Vector2(-20f, (float)(20 + 20 * i)), baseColor, 0f, Main.fontItemStack.MeasureString(text2) * Vector2.UnitX, Vector2.One, -1f, 2f);
                }
                this.DrawWaterChoices(sb, (rect.TopLeft() + new Vector2((float)(rect.Width / 2 - 58), 90f)).ToPoint(), Main.MouseScreen.ToPoint());
            }
            public override void ToggleActive(bool tickedOn)
            {
                if (tickedOn)
                {
                    this.hoveredButton = -1;
                }
            }
            public override bool UsingMap()
            {
                return this.inUI;
            }
        }
        private const float CameraMaxFrame = 5f;
        private const float CameraMaxWait = 60f;
        private static Dictionary<int, MapCaptureInterface.MapCaptureInterfaceMode> Modes = MapCaptureInterface.FillModes();
        public bool Active;
        public static bool JustActivated = false;
        public int SelectedMode;
        public int HoveredMode;
        public static bool EdgeAPinned = false;
        public static bool EdgeBPinned = false;
        public static Point EdgeA = default(Point);
        public static Point EdgeB = default(Point);
        public static bool CameraLock = false;
        private static float CameraFrame = 0f;
        private static float CameraWaiting = 0f;
        private static MapCaptureSettings CameraSettings;
        private static Rectangle area;
        private static Point pasteTo;
        private static Dictionary<int, MapCaptureInterface.MapCaptureInterfaceMode> FillModes()
        {
            return new Dictionary<int, MapCaptureInterface.MapCaptureInterfaceMode>
            {

                {
                    0,
                    new MapCaptureInterface.ModeEdgeSelection()
                },

                {
                    1,
                    new MapCaptureInterface.ModeDragBounds()
                },

                {
                    2,
                    new MapCaptureInterface.ModeChangeSettings()
                }
            };
        }
        public static Rectangle GetArea()
        {
            int x = Math.Min(MapCaptureInterface.EdgeA.X, MapCaptureInterface.EdgeB.X);
            int y = Math.Min(MapCaptureInterface.EdgeA.Y, MapCaptureInterface.EdgeB.Y);
            int num = Math.Abs(MapCaptureInterface.EdgeA.X - MapCaptureInterface.EdgeB.X);
            int num2 = Math.Abs(MapCaptureInterface.EdgeA.Y - MapCaptureInterface.EdgeB.Y);
            return new Rectangle(x, y, num + 1, num2 + 1);
        }

        public void Update() //Ultimative update method
        {
            this.UpdateCamera();
            if (MapCaptureInterface.CameraLock)
            {
                return;
            }
            if (!this.Active)
            {
                return;
            }
            Main.blockMouse = true;
            if (MapCaptureInterface.JustActivated && Main.mouseLeftRelease && !Main.mouseLeft)
            {
                MapCaptureInterface.JustActivated = false;
            }
            Vector2 mouse = new Vector2((float)Main.mouseX, (float)Main.mouseY);
            if (this.UpdateButtons(mouse) && Main.mouseLeft)
            {
                return;
            }


            foreach (KeyValuePair<int, MapCaptureInterface.MapCaptureInterfaceMode> current in MapCaptureInterface.Modes)
            {
                current.Value.Selected = (current.Key == this.SelectedMode);
                current.Value.Update();
            }
            //TP
            if (MapCaptureInterface.Settings.TPPressed && Main.mouseLeftRelease && !MapCaptureInterface.JustActivated)
            {
                MapCaptureInterface.Settings.TPPressed = false;
            }

            if (MapCaptureInterface.Settings.TP && Main.mouseLeft && !MapCaptureInterface.JustActivated)
            {
                MapCaptureInterface.Settings.TPPressed = true;
                if (!Main.mapFullscreen)
                {
                    //Main.NewText(Main.MouseWorld.ToString()+""+ Main.maxTilesY, 255, 255, 0);
                    Main.player[Main.myPlayer].Teleport(Main.MouseWorld, 1, 0);

                }
                else
                {
                    //Teleportation
                    //Main.NewText(x + ":" + y, 255, 0, 0);
                    Point teleportTo;
                    GetMapCoords((int)Main.mouseX, (int)Main.mouseY, 0, out teleportTo);
                    //Main.NewText(teleportTo.X +":"+ teleportTo.Y +":::"+teleportTo.X * 16 +":"+ teleportTo.Y * 16, 255, 0, 0);
                    Main.player[Main.myPlayer].Teleport(new Vector2(teleportTo.X * 16, teleportTo.Y * 16));
                }
                MapCaptureInterface.Settings.TP = false;
            }


            //Paste
            //TODO cool looking square to see where you are pasting to
            if (MapCaptureInterface.Settings.PastePressed && Main.mouseLeftRelease && !MapCaptureInterface.JustActivated)
            {
                MapCaptureInterface.Settings.PastePressed = false;
            }

            if (MapCaptureInterface.Settings.Paste && Main.mouseLeft && !MapCaptureInterface.JustActivated)
            {
                MapCaptureInterface.Settings.PastePressed = true;
                if (!Main.mapFullscreen)
                {
                    //Main.player[Main.myPlayer].Teleport(Main.MouseWorld, 1, 0);
                    pasteTo = Main.MouseWorld.ToTileCoordinates();
                }
                else
                {
                    GetMapCoords((int)Main.mouseX, (int)Main.mouseY, 0, out pasteTo);
                    //Main.player[Main.myPlayer].Teleport(new Vector2(teleportTo.X * 16, teleportTo.Y * 16));
                }
                //Reverse paste
                //if (pasteTo.Y >= area.Y && pasteTo.Y < area.Y + area.Height && pasteTo.X >= area.X - area.Width && pasteTo.X < area.X + area.Width) //Check for intersection
                if ((pasteTo.Y > area.Y && pasteTo.X > area.X - area.Width || pasteTo.X > area.X && pasteTo.Y >= area.Y) && pasteTo.Y < area.Y + area.Height && pasteTo.X < area.X + area.Width) //Check for intersection
                {
                    int diffX = pasteTo.X - area.X;
                    int diffY = pasteTo.Y - area.Y;
                    for (int x = area.X + area.Width - 1; x >= area.X; x--)
                    {
                        for (int y = area.Y + area.Height -1; y >=  area.Y; y--)
                        {
                            Main.tile[x + diffX, y + diffY].CopyFrom(Main.tile[x, y]);
                            //Copy the dummies
                            if (GameContent.Tile_Entities.TETrainingDummy.ValidTile(x, y))
                            {
                                GameContent.Tile_Entities.TETrainingDummy.Place(x + diffX, y + diffY);
                                //Main.NewText("X: "+x+"  Y: "+ y);
                            }
                        }
                    }
                    //Main.NewText("PY: " + pasteTo.Y + "  AY: " + area.Y+ "  PX: " + pasteTo.X + "  AX: " + area.X);
                }
                else {
                    int diffX = pasteTo.X - area.X;
                    int diffY = pasteTo.Y - area.Y;
                    for (int x = area.X; x < area.X + area.Width; x++)
                    {
                        for (int y = area.Y; y < area.Y + area.Height; y++)
                        {
                            Main.tile[x + diffX, y + diffY].CopyFrom(Main.tile[x, y]);
                            //Copy the dummies
                            if (GameContent.Tile_Entities.TETrainingDummy.ValidTile(x, y))
                            {
                                GameContent.Tile_Entities.TETrainingDummy.Place(x + diffX, y + diffY);
                                //Main.NewText("X: "+x+"  Y: "+ y);
                            }
                        }
                    }
                }
                MapCaptureInterface.Settings.Paste = false;
            }
        }
        public void Draw(SpriteBatch sb)
        {
            if (!this.Active)
            {
                return;
            }
            foreach (MapCaptureInterface.MapCaptureInterfaceMode current in MapCaptureInterface.Modes.Values)
            {
                current.Draw(sb);
            }
            Main.mouseText = false;
            Main.instance.GUIBarsDraw();
            this.DrawButtons(sb);
            Main.instance.DrawMouseOver();
            Utils.DrawBorderStringBig(sb, "Terraria Map Editor", new Vector2((float)Main.screenWidth * 0.5f, 100f), Color.White, 1f, 0.5f, 0.5f, -1);
            Utils.DrawCursorSingle(sb, Main.cursorColor, float.NaN, Main.cursorScale, default(Vector2), 0, 0);
            this.DrawCameraLock(sb);
        }
        public void ToggleCamera(bool On = true)
        {
            if (MapCaptureInterface.CameraLock)
            {
                return;
            }
            bool active = this.Active;
            this.Active = (MapCaptureInterface.Modes.ContainsKey(this.SelectedMode) && On);
            if (active != this.Active)
            {
                Main.PlaySound(12, -1, -1, 1);
            }
            foreach (KeyValuePair<int, MapCaptureInterface.MapCaptureInterfaceMode> current in MapCaptureInterface.Modes)
            {
                current.Value.ToggleActive(this.Active && current.Key == this.SelectedMode);
            }
            if (On && !active)
            {
                MapCaptureInterface.JustActivated = true;
            }
        }
        private bool UpdateButtons(Vector2 mouse)
        {
            this.HoveredMode = -1;
            bool flag = !Main.graphics.IsFullScreen;
            for (int i = 0; i < numberOfButtons; i++)
            {
                Rectangle rectangle = new Rectangle(24 + 46 * i, 24, 42, 42);
                if (rectangle.Contains(mouse.ToPoint()))
                {
                    this.HoveredMode = i;
                    bool flag2 = Main.mouseLeft && Main.mouseLeftRelease;
                    int num2 = 0;
                    if (i == num2++ && flag2 && MapCaptureInterface.EdgeAPinned && MapCaptureInterface.EdgeBPinned) //Edit
                    {
                        area = MapCaptureInterface.GetArea();
                        Main.BlackFadeIn = 150;
                        //Lighting.BlackOut();
                        /*MapCaptureSettings captureSettings = new MapCaptureSettings();
                        Point point = Main.screenPosition.ToTileCoordinates();
                        Point point2 = (Main.screenPosition + new Vector2((float)Main.screenWidth, (float)Main.screenHeight)).ToTileCoordinates();
                        captureSettings.Area = new Rectangle(point.X, point.Y, point2.X - point.X + 1, point2.Y - point.Y + 1);
                        captureSettings.Biome = MapCaptureBiome.Biomes[MapCaptureInterface.Settings.BiomeChoice];
                        captureSettings.MapCaptureBackground = !MapCaptureInterface.Settings.TransparentBackground;
                        captureSettings.MapCaptureEntities = false;
                        captureSettings.UseScaling = false;
                        captureSettings.MapCaptureMech = Main.player[Main.myPlayer].inventory[Main.player[Main.myPlayer].selectedItem].mech;
                        MapCaptureInterface.StartCamera(captureSettings);*/
                        //Main.NewText("copied");
                    }
                    if (i == num2++ && flag2) //Edit
                    {
                        MapCaptureInterface.Settings.Paste = !MapCaptureInterface.Settings.Paste;
                        /*MapCaptureInterface.StartCamera(new MapCaptureSettings
                        {
                            Area = MapCaptureInterface.GetArea(),
                            Biome = MapCaptureBiome.Biomes[MapCaptureInterface.Settings.BiomeChoice],
                            MapCaptureBackground = !MapCaptureInterface.Settings.TransparentBackground,
                            MapCaptureEntities = false,
                            UseScaling = false,
                            MapCaptureMech = Main.player[Main.myPlayer].inventory[Main.player[Main.myPlayer].selectedItem].mech
                        });*/
                    }
                    if (i == num2++ && flag2)
                    {
                        MapCaptureInterface.Settings.TP = !MapCaptureInterface.Settings.TP;
                    }
                    if (i == num2++ && flag2 && this.SelectedMode != 1)
                    {
                        Main.NewText("OI");
                        this.SelectedMode = 1;
                        this.ToggleCamera(true);
                    }
                    if (i == num2++ && flag2)
                    {
                        MapCaptureInterface.ResetFocus();
                    }
                    if (i == num2++ && flag2 && Main.mapEnabled)
                    {
                        Main.mapFullscreen = !Main.mapFullscreen;
                    }
                    if (i == num2++ && flag2 && this.SelectedMode != 2)
                    {
                        this.SelectedMode = 2;
                        this.ToggleCamera(true);
                    }
                    if (i == num2++ && flag2 && flag)
                    {
                        string fileName = Path.Combine(Main.SavePath, "Captures");
                        Process.Start(fileName);
                    }
                    if (i == num2++ && flag2)
                    {
                        this.ToggleCamera(false);
                        Main.blockMouse = true;
                        Main.mouseLeftRelease = false;
                    }
                    return true;
                }
            }
            return false;
        }
        private void DrawButtons(SpriteBatch sb)
        {
            //if(Main.startCopy);
            new Vector2((float)Main.mouseX, (float)Main.mouseY);
            //int num = numberOfButtons;
            for (int i = 0; i < numberOfButtons; i++)
            {
                Texture2D texture2D = Main.inventoryBackTexture;
                float num2 = 0.8f;
                Vector2 vector = new Vector2((float)(24 + 46 * i), 24f);
                Color color = Main.inventoryBack * 0.8f;
                /*if (this.SelectedMode == 0 && i == 2) //Background of buttons
                {
                    texture2D = Main.inventoryBack14Texture;
                }
                else if (this.SelectedMode == 1 && i == 3)
                {
                    texture2D = Main.inventoryBack14Texture;
                }
                else if (this.SelectedMode == 2 && i == 6)
                {
                    texture2D = Main.inventoryBack14Texture;
                }
                else if (i >= 2 && i <= 3)
                {
                    texture2D = Main.inventoryBack2Texture;
                }*/
                if (i == 1 && MapCaptureInterface.Settings.Paste) //Paste button
                {
                    texture2D = Main.inventoryBack2Texture;
                }
                else if (i == 2 && MapCaptureInterface.Settings.TP) //TP button
                {
                    texture2D = Main.inventoryBack2Texture;
                }

                sb.Draw(texture2D, vector, null, color, 0f, default(Vector2), num2, SpriteEffects.None, 0f);
                switch (i) //Button icons
                {
                    case 0:
                        texture2D = Main.cursorTextures[8]; //Copy
                        break;
                    case 1:
                        texture2D = Main.cursorTextures[9]; //Paste
                        break;
                    case 2:
                        texture2D = Main.itemTexture[1326];
                        break;
                        /*
                        case 3:
                        case 4:
                            texture2D = Main.cameraTexture[i];
                            break;
                        case 5:
                            texture2D = (Main.mapFullscreen ? Main.mapIconTexture[0] : Main.mapIconTexture[4]);
                            break;
                        case 6:
                            texture2D = Main.cameraTexture[1];
                            break;
                        case 7:
                            texture2D = Main.cameraTexture[6];
                            break;
                        case 8:
                            texture2D = Main.cameraTexture[5];
                            break;*/
                }
                sb.Draw(texture2D, vector + new Vector2(26f) * num2, null, Color.White, 0f, texture2D.Size() / 2f, 1f, SpriteEffects.None, 0f);
                bool flag = false;
                //int num3 = i;
                /*if (num3 != 1) //Cooldown/crossed out
                {
                    switch (num3)
                    {
                        case 5:
                            if (!Main.mapEnabled)
                            {
                                flag = true;
                            }
                            break;
                        case 7:
                            if (Main.graphics.IsFullScreen)
                            {
                                flag = true;
                            }
                            break;
                    }
                } else*/
                if ((!MapCaptureInterface.EdgeAPinned || !MapCaptureInterface.EdgeBPinned) && i == 0)
                {
                    flag = true;
                }
                if (flag)
                {
                    sb.Draw(Main.cdTexture, vector + new Vector2(26f) * num2, null, Color.White * 0.65f, 0f, Main.cdTexture.Size() / 2f, 1f, SpriteEffects.None, 0f);
                }
            }
            string text = ""; //Text when hovering
            switch (this.HoveredMode)
            {
                case -1:
                    break;
                case 0:
                    text = "Copy";
                    break;
                case 1:
                    text = "Paste";
                    break;
                case 2:
                    text = "Teleport";
                    break;
                /*
            case 3:
                text = Lang.inter[70];
                break;
            case 4:
                text = Lang.inter[78];
                break;
            case 5:
                text = (Main.mapFullscreen ? Lang.inter[109] : Lang.inter[108]);
                break;
            case 6:
                text = Lang.inter[68];
                break;
            case 7:
                text = Lang.inter[110];
                break;
            case 8:
                text = Lang.inter[71];
                break;*/
                default:
                    text = "???";
                    break;
            }
            int hoveredMode = this.HoveredMode;
            /*if (hoveredMode != 1)
            {
                switch (hoveredMode)
                {
                    case 5:
                        if (!Main.mapEnabled)
                        {
                            text = text + "\n" + Lang.inter[114];
                        }
                        break;
                    case 7:
                        if (Main.graphics.IsFullScreen)
                        {
                            text = text + "\n" + Lang.inter[113];
                        }
                        break;
                }
            }
            else if (!MapCaptureInterface.EdgeAPinned || !MapCaptureInterface.EdgeBPinned)
            {
                text = text + "\n" + Lang.inter[112];
            }*/
            if (text != "")
            {
                Main.instance.MouseText(text, 0, 0);
            }
        }
        public static bool GetMapCoords(int PinX, int PinY, int Goal, out Point result)
        {
            if (!Main.mapFullscreen)
            {
                result = new Point(-1, -1);
                return false;
            }
            int arg_33_0 = Main.maxTilesX / Main.textureMaxWidth;
            int arg_3F_0 = Main.maxTilesY / Main.textureMaxHeight;
            float num = 10f;
            float num2 = 10f;
            float num3 = (float)(Main.maxTilesX - 10);
            float num4 = (float)(Main.maxTilesY - 10);
            float mapFullscreenScale = Main.mapFullscreenScale;
            float num5 = (float)Main.screenWidth / (float)Main.maxTilesX * 0.8f;
            if (Main.mapFullscreenScale < num5)
            {
                Main.mapFullscreenScale = num5;
            }
            if (Main.mapFullscreenScale > 16f)
            {
                Main.mapFullscreenScale = 16f;
            }
            mapFullscreenScale = Main.mapFullscreenScale;
            if (Main.mapFullscreenPos.X < num)
            {
                Main.mapFullscreenPos.X = num;
            }
            if (Main.mapFullscreenPos.X > num3)
            {
                Main.mapFullscreenPos.X = num3;
            }
            if (Main.mapFullscreenPos.Y < num2)
            {
                Main.mapFullscreenPos.Y = num2;
            }
            if (Main.mapFullscreenPos.Y > num4)
            {
                Main.mapFullscreenPos.Y = num4;
            }
            float num6 = Main.mapFullscreenPos.X;
            float num7 = Main.mapFullscreenPos.Y;
            num6 *= mapFullscreenScale;
            num7 *= mapFullscreenScale;
            float num8 = -num6 + (float)(Main.screenWidth / 2);
            float num9 = -num7 + (float)(Main.screenHeight / 2);
            num8 += num * mapFullscreenScale;
            num9 += num2 * mapFullscreenScale;
            float num10 = (float)(Main.maxTilesX / 840);
            num10 *= Main.mapFullscreenScale;
            float num11 = num8;
            float num12 = num9;
            float num13 = (float)Main.mapTexture.Width;
            float num14 = (float)Main.mapTexture.Height;
            if (Main.maxTilesX == 8400)
            {
                num10 *= 0.999f;
                num11 -= 40.6f * num10;
                num12 = num9 - 5f * num10;
                num13 -= 8.045f;
                num13 *= num10;
                num14 += 0.12f;
                num14 *= num10;
                if ((double)num10 < 1.2)
                {
                    num14 += 1f;
                }
            }
            else if (Main.maxTilesX == 6400)
            {
                num10 *= 1.09f;
                num11 -= 38.8f * num10;
                num12 = num9 - 3.85f * num10;
                num13 -= 13.6f;
                num13 *= num10;
                num14 -= 6.92f;
                num14 *= num10;
                if ((double)num10 < 1.2)
                {
                    num14 += 2f;
                }
            }
            else if (Main.maxTilesX == 6300)
            {
                num10 *= 1.09f;
                num11 -= 39.8f * num10;
                num12 = num9 - 4.08f * num10;
                num13 -= 26.69f;
                num13 *= num10;
                num14 -= 6.92f;
                num14 *= num10;
                if ((double)num10 < 1.2)
                {
                    num14 += 2f;
                }
            }
            else if (Main.maxTilesX == 4200)
            {
                num10 *= 0.998f;
                num11 -= 37.3f * num10;
                num12 -= 1.7f * num10;
                num13 -= 16f;
                num13 *= num10;
                num14 -= 8.31f;
                num14 *= num10;
            }
            if (Goal == 0)
            {
                int num15 = (int)((-num8 + (float)PinX) / mapFullscreenScale + num);
                int num16 = (int)((-num9 + (float)PinY) / mapFullscreenScale + num2);
                bool flag = false;
                if ((float)num15 < num)
                {
                    flag = true;
                }
                if ((float)num15 >= num3)
                {
                    flag = true;
                }
                if ((float)num16 < num2)
                {
                    flag = true;
                }
                if ((float)num16 >= num4)
                {
                    flag = true;
                }
                if (!flag)
                {
                    result = new Point(num15, num16);
                    return true;
                }
                result = new Point(-1, -1);
                return false;
            }
            else
            {
                if (Goal == 1)
                {
                    Vector2 value = new Vector2(num8, num9);
                    Vector2 value2 = new Vector2((float)PinX, (float)PinY) * mapFullscreenScale - new Vector2(10f * mapFullscreenScale);
                    result = (value + value2).ToPoint();
                    return true;
                }
                result = new Point(-1, -1);
                return false;
            }
        }
        private static void ConstraintPoints()
        {
            int offScreenTiles = Lighting.offScreenTiles;
            if (MapCaptureInterface.EdgeAPinned)
            {
                MapCaptureInterface.PointWorldClamp(ref MapCaptureInterface.EdgeA, offScreenTiles);
            }
            if (MapCaptureInterface.EdgeBPinned)
            {
                MapCaptureInterface.PointWorldClamp(ref MapCaptureInterface.EdgeB, offScreenTiles);
            }
        }
        private static void PointWorldClamp(ref Point point, int fluff)
        {
            if (point.X < fluff)
            {
                point.X = fluff;
            }
            if (point.X > Main.maxTilesX - 1 - fluff)
            {
                point.X = Main.maxTilesX - 1 - fluff;
            }
            if (point.Y < fluff)
            {
                point.Y = fluff;
            }
            if (point.Y > Main.maxTilesY - 1 - fluff)
            {
                point.Y = Main.maxTilesY - 1 - fluff;
            }
        }
        public bool UsingMap()
        {
            return MapCaptureInterface.CameraLock || MapCaptureInterface.Modes[this.SelectedMode].UsingMap();
        }
        public static void ResetFocus()
        {
            MapCaptureInterface.EdgeAPinned = false;
            MapCaptureInterface.EdgeBPinned = false;
            MapCaptureInterface.EdgeA = new Point(-1, -1);
            MapCaptureInterface.EdgeB = new Point(-1, -1);
        }
        public void Scrolling()
        {
            int num = (Main.mouseState.ScrollWheelValue - Main.oldMouseWheel) / 120;
            num %= 30;
            if (num < 0)
            {
                num += 30;
            }
            int selectedMode = this.SelectedMode;
            this.SelectedMode -= num;
            while (this.SelectedMode < 0)
            {
                this.SelectedMode += 2;
            }
            while (this.SelectedMode > 2)
            {
                this.SelectedMode -= 2;
            }
            if (this.SelectedMode != selectedMode)
            {
                Main.PlaySound(12, -1, -1, 1);
            }
        }
        private void UpdateCamera()
        {
            if (MapCaptureInterface.CameraLock && MapCaptureInterface.CameraFrame == 4f)
            {
                MapCaptureManager.Instance.MapCapture(MapCaptureInterface.CameraSettings);
            }
            MapCaptureInterface.CameraFrame += (float)MapCaptureInterface.CameraLock.ToDirectionInt();
            if (MapCaptureInterface.CameraFrame < 0f)
            {
                MapCaptureInterface.CameraFrame = 0f;
            }
            if (MapCaptureInterface.CameraFrame > 5f)
            {
                MapCaptureInterface.CameraFrame = 5f;
            }
            if (MapCaptureInterface.CameraFrame == 5f)
            {
                MapCaptureInterface.CameraWaiting += 1f;
            }
            if (MapCaptureInterface.CameraWaiting > 60f)
            {
                MapCaptureInterface.CameraWaiting = 60f;
            }
        }
        private void DrawCameraLock(SpriteBatch sb)
        {
            if (MapCaptureInterface.CameraFrame == 0f)
            {
                return;
            }
            sb.Draw(Main.magicPixel, new Rectangle(0, 0, Main.screenWidth, Main.screenHeight), new Rectangle?(new Rectangle(0, 0, 1, 1)), Color.Black * (MapCaptureInterface.CameraFrame / 5f));
            if (MapCaptureInterface.CameraFrame != 5f)
            {
                return;
            }
            float num = MapCaptureInterface.CameraWaiting - 60f + 5f;
            if (num <= 0f)
            {
                return;
            }
            num /= 5f;
            float num2 = MapCaptureManager.Instance.GetProgress() * 100f;
            if (num2 > 100f)
            {
                num2 = 100f;
            }
            string text = num2.ToString("##") + " ";
            string text2 = "/ 100%";
            Vector2 vector = Main.fontDeathText.MeasureString(text);
            Vector2 vector2 = Main.fontDeathText.MeasureString(text2);
            Vector2 value = new Vector2(-vector.X, -vector.Y / 2f);
            Vector2 value2 = new Vector2(0f, -vector2.Y / 2f);
            ChatManager.DrawColorCodedStringWithShadow(sb, Main.fontDeathText, text, new Vector2((float)Main.screenWidth, (float)Main.screenHeight) / 2f + value, Color.White * num, 0f, Vector2.Zero, Vector2.One, -1f, 2f);
            ChatManager.DrawColorCodedStringWithShadow(sb, Main.fontDeathText, text2, new Vector2((float)Main.screenWidth, (float)Main.screenHeight) / 2f + value2, Color.White * num, 0f, Vector2.Zero, Vector2.One, -1f, 2f);
        }
        public static void StartCamera(MapCaptureSettings settings)
        {
            Main.PlaySound(40, -1, -1, 1);
            MapCaptureInterface.CameraSettings = settings;
            MapCaptureInterface.CameraLock = true;
            MapCaptureInterface.CameraWaiting = 0f;
        }
        public static void EndCamera()
        {
            MapCaptureInterface.CameraLock = false;
        }
    }
}
