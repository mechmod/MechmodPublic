using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
namespace Terraria.Graphics.MapCapture
{
    internal class MapCaptureManager
    {
        public static MapCaptureManager Instance = new MapCaptureManager();
        private MapCaptureInterface _interface;
        private MapCaptureCamera _camera;
        public bool IsCapturing
        {
            get
            {
                return this._camera.IsCapturing;
            }
        }
        public bool Active
        {
            get
            {
                return this._interface.Active;
            }
            set
            {
                if (Main.MapUIDisabled)
                {
                    return;
                }
                if (this._interface.Active != value)
                {
                    this._interface.ToggleCamera(value);
                }
            }
        }
        public bool UsingMap
        {
            get
            {
                return this.Active && this._interface.UsingMap();
            }
        }
        public MapCaptureManager()
        {
            this._interface = new MapCaptureInterface();
            this._camera = new MapCaptureCamera(Main.instance.GraphicsDevice);
        }
        public void Scrolling()
        {
            this._interface.Scrolling();
        }
        public void Update()
        {
            this._interface.Update();
        }
        public void Draw(SpriteBatch sb)
        {
            this._interface.Draw(sb);
        }
        public float GetProgress()
        {
            return this._camera.GetProgress();
        }
        public void MapCapture()
        {
            this.MapCapture(new MapCaptureSettings
            {
                Area = new Rectangle(2660, 100, 1000, 1000),
                UseScaling = false
            });
        }
        public void MapCapture(MapCaptureSettings settings)
        {
            this._camera.MapCapture(settings);
        }
        public void DrawTick()
        {
            this._camera.DrawTick();
        }
    }
}
