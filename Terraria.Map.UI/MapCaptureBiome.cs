using System;
namespace Terraria.Graphics.MapCapture
{
    public class MapCaptureBiome
    {
        public enum TileColorStyle
        {
            Normal,
            Jungle,
            Crimson,
            Corrupt,
            Mushroom
        }
        public static MapCaptureBiome[] Biomes;
        public readonly int WaterStyle;
        public readonly int BackgroundIndex;
        public readonly int BackgroundIndex2;
        public readonly MapCaptureBiome.TileColorStyle TileColor;
        public MapCaptureBiome(int backgroundIndex, int backgroundIndex2, int waterStyle, MapCaptureBiome.TileColorStyle tileColorStyle = MapCaptureBiome.TileColorStyle.Normal)
        {
            this.BackgroundIndex = backgroundIndex;
            this.BackgroundIndex2 = backgroundIndex2;
            this.WaterStyle = waterStyle;
            this.TileColor = tileColorStyle;
        }
        static MapCaptureBiome()
        {
            // Note: this type is marked as 'beforefieldinit'.
            MapCaptureBiome[] array = new MapCaptureBiome[12];
            array[0] = new MapCaptureBiome(0, 0, 0, MapCaptureBiome.TileColorStyle.Normal);
            array[2] = new MapCaptureBiome(1, 2, 2, MapCaptureBiome.TileColorStyle.Corrupt);
            array[3] = new MapCaptureBiome(3, 0, 3, MapCaptureBiome.TileColorStyle.Jungle);
            array[4] = new MapCaptureBiome(6, 2, 4, MapCaptureBiome.TileColorStyle.Normal);
            array[5] = new MapCaptureBiome(7, 4, 5, MapCaptureBiome.TileColorStyle.Normal);
            array[6] = new MapCaptureBiome(2, 1, 6, MapCaptureBiome.TileColorStyle.Normal);
            array[7] = new MapCaptureBiome(9, 6, 7, MapCaptureBiome.TileColorStyle.Mushroom);
            array[8] = new MapCaptureBiome(0, 0, 8, MapCaptureBiome.TileColorStyle.Normal);
            array[10] = new MapCaptureBiome(8, 5, 10, MapCaptureBiome.TileColorStyle.Crimson);
            MapCaptureBiome.Biomes = array;
        }
    }
}
