Mechmod Public Repo
====
This is the *public* repository for Mechmod. Each file changed has a .patch here to avoid distributing the entire source of Terraria, excluding the Art folders and completely new files.

This repo will be used for Releases, Issues and maybe even a wiki.
Please report bugs etc in the Issue Tracker, in the pane to the right. (note: you'll need to make an account to do this. if you don't want to, shoot us a PM on the forums and we'll transfer it to here under one of our accounts) This makes it easier for us to manage updates, requests and bugs.

Updates
---
This repo will only be updated every release or so, not every time we change anything. Don't measure our progress by this repo; there are usually a ton of things going on behind the scenes, but sometimes we forget to update the .patches.

---
###Contributing

If you'd like to make a major contribution to Mechmod's code, message us on the forums and we'll invite you to the private repository with complete files. Otherwise, if you are changing something small or are contributing to a non-patch file (e.g. the art, MechmodOptions.cs, others) feel free to contribute directly to this repo and we'll update our internal one from there. Thanks!
