﻿using System.Collections.Generic;
using System.IO;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Windows.Forms;

namespace Terraria.Utilities
{
    static class ConfigHandler //I wanted to use the name "ConfigMagic" but sanity won...
    {
        /// <summary>
        /// Remember to cast them...
        /// </summary>
        public static Dictionary<string, object> configOptions = new Dictionary<string, object>();

        private static StreamReader fileReader;

        private static string _path;
        static ConfigHandler()
        {
            _path = Main.SavePath + "/MechmodConfig.txt";
            if (!File.Exists(_path))
                File.CreateText(_path).Close();
        }
        public static void configFileSetup(bool force = false)
        {
            if (force)
            {
                File.Delete(_path);
                File.CreateText(_path).Close();
            }
            configOptions.Add("configIsSetup", false);
            readConfig();

            /*if (b("configIsSetup") == true)// && ((int)f("build") > System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build || ((int)f("build") == System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build && (int)f("buildRev") > System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Revision)))
            {
                //If the game /thinks/ that it is setup, but the version has been updated, reset the file.
                configOptions["configIsSetup"] = false;
                File.Delete(_path);
                File.CreateText(_path).Close();
            }*/
            if (getBool("configIsSetup") == true && !force)
            {
                MechmodOptions.LoadSettings();
                return;
            }
            else
            {

            }

            //Don't actually need these anymore :)
            /*
            configOptions.Add("camSpeed", 4);
			configOptions.Add("dummyGhostThick", false);
			configOptions.Add("dummyGhostBright", false);
			configOptions.Add("showDummyGhosts", false);
			configOptions.Add("slomoRate", 2);
            configOptions.Add("brightYellowWire", false); //For later
			configOptions.Add("brightGreenWire", false);
			configOptions.Add("brightRedWire", false);
			configOptions.Add("blueWireBright", false);
			configOptions.Add("actuatorsBright", false);
			configOptions.Add("redWireBrightness", 1);
			configOptions.Add("blueWireBrightness", 1);
			configOptions.Add("greenWireBrightness", 1);
			configOptions.Add("actuatorBrightness", 1);
			configOptions.Add("alwaysShowWires", false);
            configOptions.Add("infiniteWire", false);
			configOptions.Add("infiniteActuators", false);
			configOptions.Add("godmode", false);
			configOptions.Add("infiniteHealth", false);
			configOptions.Add("infiniteMana", false); //aka enableLastPrisimFun
			configOptions.Add("UFOSpeed", 1f);
			*/

            WriteConfig();
            fileReader.Close();
        }
        public static void readConfig() //Please work!
        {
            fileReader = new StreamReader(_path, true);

			string datLineRightNaow;

			datLineRightNaow = fileReader.ReadLine();
			if(datLineRightNaow[0] == '+')
			{
				HotbarButton._cachedSaveString = datLineRightNaow.Substring(1);
			}
			else
			{
				HotbarButton._cachedSaveString = "Screen Lock,Dummy Ghosts,Slow Motion,Pause,!,Godmode,!,!,!,!,";
				fileReader.BaseStream.Seek(0, SeekOrigin.Begin);
			}
			//This isn't working well considering that hotbar names (a) contain spaces (b) are case-sensitive (c) picky lil shits. so im gonna just be lazy and do this :(
            while ((datLineRightNaow = fileReader.ReadLine()) != null)
            {
                if (datLineRightNaow.Length > 1) //The length should be over 1...that means that empty lines will get ignored
                {
					if (!ReadSavedLocations(datLineRightNaow))// && ReadSavedHotbuttons(currentLine))
					{
						string[] currentLine = datLineRightNaow.Replace(" ", "").Split('=');

						if (configOptions.ContainsKey(currentLine[0]))
							configOptions[currentLine[0]] = currentLine[1];
						else
							configOptions.Add(currentLine[0], currentLine[1]);
					}
                }
            }
            fileReader.Close();
        }
		
        public static void WriteConfig()
        {
            File.Delete(_path);
            //A really good idea, creating and destroying an object every time the function is called
            using (StreamWriter fileWriter = File.CreateText(_path))
            {
				fileWriter.WriteLine("+" + HotbarButton.GetSaveString());
                foreach (KeyValuePair<string, object> entry in configOptions)
                {
					fileWriter.WriteLine(entry.Key + " = " + entry.Value.ToString().ToLower());
                }
            }
        }

        //What the heck? C# returns `True` for .ToString() but only accepts lowercase for casting back ... Floats didn't work either.
        public static bool getBool(string key, bool defaultValue = false)
        {
            object returnValue;
            if (configOptions.TryGetValue(key, out returnValue))
            {
                return Convert.ToBoolean(returnValue);
            }
            else
            {
                configOptions[key] = defaultValue;
                return defaultValue;
            }
        }
        public static float getFloat(string key, float defaultValue = 1)
        {
            object returnValue;
            if (configOptions.TryGetValue(key, out returnValue))
            {
                return Convert.ToSingle(returnValue);
            }
            else
            {
                configOptions[key] = defaultValue;
                return defaultValue;
            }
        }


		public static bool ReadSavedLocations(string dataLine)
		{
			string[] currentLine = dataLine.Replace(" ", "").Split('=');
			if (currentLine[0].Substring(0, 3) == "tp-")
			{
				string key = currentLine[0].Substring(3);
				string[] pos = currentLine[1].Split(',');
				if (Main.savedLocations.ContainsKey(key))
					Main.savedLocations[key] = new Vector2(Convert.ToInt32(pos[0]), Convert.ToInt32(pos[1]));
				else
					Main.savedLocations.Add(key, new Vector2(Convert.ToInt32(pos[0]), Convert.ToInt32(pos[1])));
				return true;
			}
			return false;
		}
		

	}
}