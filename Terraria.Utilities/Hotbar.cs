﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.InteropServices;
using Terraria.Mechmod;

namespace Terraria.Utilities
{
	public class HotbarTexture
	{
		public Texture2D texture;
		public Rectangle sourceRect;
		public HotbarTexture(Texture2D tex, Rectangle rect)
		{
			texture = tex;
			sourceRect = rect;
		}
		public HotbarTexture(Texture2D tex)
		{
			texture = tex;
			sourceRect = new Rectangle(0, 0, tex.Width, tex.Height);
		}
		public HotbarTexture(Rectangle rect)
		{
			texture = Main.hotbarTextures;
			sourceRect = rect;
		}
		public HotbarTexture(int x, int y, int w, int h)
		{
			texture = Main.hotbarTextures;
			sourceRect = new Rectangle(x, y, w, h);
		}
	}
	public class HotbarButton
	{
		public static Color[] colors =
		{
			new Color(98, 98, 227, 255),
			new Color(98, 98, 98, 255),
			new Color(98, 227, 98, 255),
			new Color(98, 227, 227, 255),
			new Color(227, 98, 98, 255),
			new Color(227, 98, 227, 255),
			new Color(227, 227, 98, 255),
			new Color(227, 227, 227, 255),
		};
		[DllImport("kernel32.dll", SetLastError = true)]
		internal static extern int AllocConsole();
		//Static
		const int MAX_BUTTONS = 10;
		const int PADDING = 4;
		const float minTransparency = 0.6f;
		const float maxTransparency = 0.95f; // a bit of leeway - floating point innacuracies can mean n > max, which results in the bytes 'looping' over to dark colors
		const float transparencyIncrement = 0.02f;
		public static float transparency = 1.2f;
		const float clickDim = 0.8f;
		const float colorMultiplier = 0.8f;
		static Point buttonSize = new Point(40, 40);
		static Rectangle sourceRectUp = new Rectangle(0, 0, buttonSize.X, buttonSize.Y);
		static Rectangle sourceRectDown = new Rectangle(buttonSize.X, 0, buttonSize.X, buttonSize.Y);
		public static Texture2D hotbarBoxTexture;
		public static Vector2 position;
		public static HotbarButton[] hotbarButtons = new HotbarButton[MAX_BUTTONS];
		public static int hcolorIndex;
		public static int submenu;
		public static bool mouseHover = false;
		internal static string _cachedSaveString = "";
		//Instance
		HotbarTexture iconTexture;
		Action callback;
		Func<bool> isActive;
		string nameIndex;
		int index; //0,1,2,3,4,5,6,7,8,9 according to position within hotbar
		public bool down = false;

		public HotbarButton(int pos)
		{
			this.index = pos;
			this.nameIndex = "!";
		}
		public static void Initialize(Texture2D tex)
		{
			hcolorIndex = 0;
			hotbarBoxTexture = tex;
			position = new Vector2(PADDING, Main.screenHeight - tex.Height - PADDING);
			for (int i = 0; i < MAX_BUTTONS; i++)
			{
				hotbarButtons[i] = new HotbarButton(i);
			}
			
		}
		public static void Draw(SpriteBatch sb)
		{
			//Console.Clear();
			//Console.SetCursorPosition(0, 0);
			//Console.WriteLine($"{position}, {PADDING}, {new Rectangle(0 * (buttonSize.X + PADDING) + (int)position.X, (int)position.Y, buttonSize.X, buttonSize.Y)} ");
			bool click = Main.mouseLeft && Main.mouseLeftRelease;
			mouseHover = new Rectangle((int)position.X, (int)position.Y, MAX_BUTTONS * (buttonSize.X + PADDING), buttonSize.Y).Contains(new Point(Main.mouseX, Main.mouseY));

			//Update transparency
			if (mouseHover)
			{ 
				Main.blockMouse = true;
				if (transparency < maxTransparency)
					transparency += transparencyIncrement * 3;
			}
			else if (transparency > minTransparency)
			{
				transparency -= transparencyIncrement;
			}

			//Loop through buttons and draw/process each one
			//					 hehe
			foreach (HotbarButton butt in hotbarButtons)
			{
				//Area of the button
				Rectangle area = new Rectangle(butt.index * (buttonSize.X + PADDING) + (int)position.X, (int)position.Y, buttonSize.X, buttonSize.Y);
				bool mouseOver = area.Contains(new Point(Main.mouseX, Main.mouseY));
				
				//Update

				SpriteEffects effects = SpriteEffects.None;
				Rectangle sourceRect = sourceRectUp;
				float darkness = 1f;
				float buttonTransparency = transparency;
				//If button is active (e.g. screen is locked for screenLock button)
				if(butt.isActive?.Invoke() ?? false)
				{
					sourceRect = sourceRectDown;
					darkness *= 1.2f;
					buttonTransparency = Math.Min(buttonTransparency + 0.15f, 1f);
				}
				// If mouse is being held over it (or hotkey is pressed) 
				if((mouseOver && Main.mouseLeft) || butt.down)
				{
					effects = SpriteEffects.FlipVertically;
					darkness *= 0.8f;
				}


				int oldSubmenu = submenu;

				//Invoke click callback (unless in edit mode)
				if (mouseOver && click)
				{
					if(Main.isDrawingMechmodInterface)
					{
						if (Main.mouseHotbutton != "")
						{
							HotbarButton.Add(butt.index, Hotbuttons.buttons[Main.mouseHotbutton]);
							Main.mouseHotbutton = "";
                        }
						else
						{
							HotbarButton.Remove(butt.index);
						}
					}
					else
						butt.callback?.Invoke();
				}
				//Check if submenu is changed, if so, update it to reflect button's position too
				if (submenu != oldSubmenu)
				{
					submenu += butt.index * 10;
				}
				if(submenu != 0)
				{
					MechmodOptions.submenuLock = true;
					MechmodOptions.inBar = false;
					MechmodOptions.submenuHover = -1;
					if (!Main.mouseLeft)
					{
						MechmodOptions.submenuBarLock = -1;
					}
					if (MechmodOptions.submenuBarLock == -1)
					{
						MechmodOptions.notBar = false;
					}
				}
				
				//Draw 

				//Draw box
				sb.Draw(hotbarBoxTexture, area, sourceRect, dim(colors[hcolorIndex]*colorMultiplier , darkness, buttonTransparency), 0f, Vector2.Zero, effects, 0);

				//Draw icon
				if (butt.iconTexture != null)
					sb.Draw(butt.iconTexture.texture, area.Center.ToVector2(), butt.iconTexture.sourceRect, dim(Color.White, darkness, buttonTransparency), 0f, butt.iconTexture.sourceRect.Size() / 2f, 1f, 0, 0);
				//Draw debug info
				if (Main.showFrameRate && butt.index == 1)
					sb.DrawString(Main.fontItemStack, $"{dim(colors[hcolorIndex] * colorMultiplier, darkness, transparency)}\n{dim(Color.White, darkness, transparency)}\ndarkness:{darkness} transparency:{transparency}", new Vector2(0, 80), Color.WhiteSmoke);
				//Draw submenu
				if(submenu / 10 == butt.index)
				{
					MechmodOptions.Submenu sub = (MechmodOptions.Submenu) (submenu % 10);
					string completelyRubbish = "";
					
					MechmodOptions.DrawSubmenus(sb, ref sub, ref completelyRubbish, new Vector2(area.X, area.Y), true);
					if (sub == MechmodOptions.Submenu.NONE)
						submenu = 0;
					else
						submenu = butt.index * 10 + (int)sub;
					if (MechmodOptions.submenuHover != -1 && MechmodOptions.submenuBarLock == -1)
					{
						MechmodOptions.submenuBarLock = MechmodOptions.submenuHover;
					}
				}
				//Check hotkey and draw it
				butt.down = false;
				if(butt.index < 4)
				{
					string s = "";
					switch (butt.index)
					{
						case 0:
							s = Main.hotkey1;
							break;
						case 1:
							s = Main.hotkey2;
							break;
						case 2:
							s = Main.hotkey3;
							break;
						case 3:
							s = Main.hotkey4;
							break;

					}
					float hotkeyTransparency = (transparency - minTransparency) / (maxTransparency - minTransparency);
					sb.DrawString(Main.fontMouseText, s, new Vector2(area.X, area.Y + area.Height - (Main.fontMouseText.MeasureString(s).Y)*0.6f + 4f), dim(Color.White, darkness, hotkeyTransparency), 0f, Vector2.Zero, 0.8f, 0, 0);
				}
            }
		}
		public static void Add(int index, HBContainer container)
		{
			hotbarButtons[index].iconTexture = container.texture;
			hotbarButtons[index].callback = container.action;
			hotbarButtons[index].isActive = container.function ?? (() => false);
			hotbarButtons[index].nameIndex = container.name;
		}
		public static int Add(HBContainer container)
		{
			//Select the first uninitialized button
			HotbarButton button;
			try
			{
				//whoo LINQ
				button = (from hotbarbutton in hotbarButtons where (hotbarbutton.callback == null && hotbarbutton.iconTexture == null) select hotbarbutton).First();
			}
			catch
			{
				//Buttons full
				return -1;
			}
			button.iconTexture = container.texture;
			button.callback = container.action;
			button.isActive = container.function ?? (() => false);
			button.nameIndex = container.name;
			return button.index;
		} 
		//clickdrag
		//sun moon
		public static void Remove(int index)
		{
			hotbarButtons[index] = new HotbarButton(index);
		}
		public static void RemoveAll()
		{
			for(int index = 0; index < MAX_BUTTONS; index++)
				hotbarButtons[index] = new HotbarButton(index);
		}
		public static void Hit(int index)
		{
			hotbarButtons[index].callback?.Invoke();
		}
		public static void UpdatePosition()
		{
			position = new Vector2(PADDING, Main.screenHeight - hotbarBoxTexture.Height - PADDING);
		}



		private static Color dimWhite(float darkness, float alpha)
		{
			return new Color(new Vector4(new Vector3(darkness * alpha), alpha) * 255);
		}
		private static Color dim(Color c, float darkness, float alpha)
		{
			Console.WriteLine("Color: {0} Darkness: {1} Alpha: {2}", c, darkness, alpha);
			float value = darkness * alpha;
			int r = Math.Min((int)(c.R * value), 255);
			int b = Math.Min((int)(c.B * value), 255);
			int g = Math.Min((int)(c.G * value), 255);
			int a = Math.Min((int)(c.A * alpha), 255);
			return new Color(r,b,g,a);
		}

		public static void ToggleSubmenu(MechmodOptions.Submenu value)
		{
			if(submenu % 10 == (int)value)
			{
				submenu = 0;
				return;
			}
			submenu = (int)value;
		}
		public static bool SubmenuDown(MechmodOptions.Submenu value)
		{
			return ((int)submenu % 10 == (int)value);
		}

		public static string GetSaveString()
		{
			string s = "";
			foreach(HotbarButton button in hotbarButtons)
			{
				s += button.nameIndex + ",";
			}
			Console.WriteLine(s);
			return s;
		}

		public static void LoadSaveString()
		{
			string[] names = _cachedSaveString.Split(',');
			for(int i = 0; i < MAX_BUTTONS; i++)
			{
				Console.WriteLine("Processing " + names[i] + "...");
				if(names[i] != "!" && Hotbuttons.buttons.ContainsKey(names[i]))
				{
					Console.WriteLine(names[i] + "suces!");
					HotbarButton.Add(i, Hotbuttons.buttons[names[i]]);
				}
			}
		}
	}
}