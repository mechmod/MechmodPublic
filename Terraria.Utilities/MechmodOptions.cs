using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using Terraria.GameContent;
using Terraria.Mechmod;
using Terraria.Social;
using Terraria.UI;
namespace Terraria.Utilities
{
    public static class MechmodOptions
    {
        public const int width = 685;
        public const int height = 440;
        public static Texture2D mechmodLogo;
        public static bool resetConfirm = false;
        static int transitionStage = 0;
        static bool easter = false;
        static Vector2 cogPosition, redWrenchPos, blueWrenchPos;
        static float cogRotation;
        static Vector2 cogVelocity;
		public static Texture2D UITextures;
		static Rectangle triangleSourceRect = new Rectangle(12, 0, 11, 12);
		static Rectangle cogSourceRect = new Rectangle(23, 0, 17, 17);
		const int buttonLeniency = 3;
		//I think an enum is the --easiest-- most maintainable thing to do here ...
		public enum Submenu
		{
			NONE,
			GODMODE,
			HOTKEYS,
			WIREOPACITY,
		};
		public static Submenu currentSubmenu;
		static Vector2 submenuPos;
		public static int submenuHover = -1;
		static int oldSubmenuHover = -1;
		public static int submenuBarLock = -1;
		static int setHotkey = -1;
		public static bool submenuLock = false;

		public static bool drawingMiscMenu = false;
        public static Color[] colors = new Color[]
        {
			new Color(25, 25, 91, 255) * 0.685f,
            new Color(25, 25, 25, 255) * 0.685f,
            new Color(25, 91, 25, 255) * 0.685f,
            new Color(25, 91, 91, 255) * 0.685f,
            new Color(91, 25, 25, 255) * 0.685f,
            new Color(91, 25, 91, 255) * 0.685f,
            new Color(91, 91, 25, 255) * 0.685f,
            new Color(91, 91, 91, 255) * 0.685f
        };
        public static Color[] colors2 = new Color[]
        {
			new Color(65, 65, 151, 255) * 0.685f,
            new Color(65, 65, 65, 255) * 0.685f,
            new Color(65, 151, 65, 255) * 0.685f,
            new Color(65, 151, 151, 255) * 0.685f,
            new Color(151, 65, 65, 255) * 0.685f,
            new Color(151, 65, 151, 255) * 0.685f,
            new Color(151, 151, 65, 255) * 0.685f,
            new Color(151, 151, 151, 255) * 0.685f,
        };
        public static int colorIndex = 0;
        public static float[] leftScale = new float[]
        {
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f
        };
        public static float[] rightScale = new float[]
        {
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f,
            0.7f
        };
        public static int leftHover = -1;
        public static int rightHover = -1;
        public static int oldLeftHover = -1;
        public static int oldRightHover = -1;
        public static int rightLock = -1;
        public static bool inBar = false;
        public static bool notBar = false;
        public static bool noSound = false;
        private static Rectangle _GUIHover = default(Rectangle);
        public static int category = -1;
        public static Vector2 valuePosition = Vector2.Zero;
        public static void Open()
        {
            Main.playerInventory = false;
            Main.editChest = false;
            Main.npcChatText = "";
            Main.PlaySound(10, -1, -1, 1);
            Main.isDrawingMechmodInterface = true;
            MechmodOptions.category = -1;
            for (int i = 0; i < MechmodOptions.leftScale.Length; i++)
            {
                MechmodOptions.leftScale[i] = 0f;
            }
            for (int j = 0; j < MechmodOptions.rightScale.Length; j++)
            {
                MechmodOptions.rightScale[j] = 0f;
            }
            MechmodOptions.leftHover = -1;
            MechmodOptions.rightHover = -1;
            MechmodOptions.oldLeftHover = -1;
            MechmodOptions.oldRightHover = -1;
            MechmodOptions.rightLock = -1;
            MechmodOptions.inBar = false;
            MechmodOptions.notBar = false;
            MechmodOptions.noSound = false;
			currentSubmenu = Submenu.NONE;
			submenuHover = -1;
			submenuBarLock = -1;
			drawingMiscMenu = false;
        }
        public static void Close()
        {
			if (setHotkey != -1)
			{
				return;
			}
			if(drawingMiscMenu)
			{
				drawingMiscMenu = false;
				Main.PlaySound(11, -1, -1, 1);
				return;
			}
			Main.isDrawingMechmodInterface = false;
			Main.PlaySound(11, -1, -1, 1);
			Recipe.FindRecipes();
			Main.playerInventory = true;
			Main.mouseHotbutton = "";
			MechmodOptions.SaveSettings();
		}
		public static void Draw(Main mainInstance, SpriteBatch sb)
		{
			string mouseText = "";
			if (Main.player[Main.myPlayer].dead && !Main.player[Main.myPlayer].ghost)
			{
				setHotkey = -1;
                MechmodOptions.Close();
                Main.playerInventory = false;
                return;
            }
			if(drawingMiscMenu)
			{
				DrawMiscMenu(sb);
				goto END;
			}
            new Vector2((float)Main.mouseX, (float)Main.mouseY); //what is this??
            bool flag = Main.mouseLeft && Main.mouseLeftRelease;
            Vector2 screenSize = new Vector2((float)Main.screenWidth, (float)Main.screenHeight);
            Vector2 menuSize = new Vector2(width, height);  //menu dimensions
            Vector2 menuPosition = screenSize / 2f - menuSize / 2f; //position lol
            int num = 20;
			int menuWidthIncrease = 0;
			MechmodOptions._GUIHover = new Rectangle((int)(menuPosition.X - (float)num), (int)(menuPosition.Y - (float)num), (int)(menuSize.X + (float)(num * 2)) + menuWidthIncrease, (int)(menuSize.Y + (float)(num * 2)));
			Utils.DrawInvBG(sb, menuPosition.X - (float)num, menuPosition.Y - (float)num, menuSize.X + (float)(num * 2) + menuWidthIncrease, menuSize.Y + (float)(num * 2), colors[colorIndex]);
            if (new Rectangle((int)menuPosition.X - num, (int)menuPosition.Y - num, (int)menuSize.X + num * 2, (int)menuSize.Y + num * 2).Contains(new Point(Main.mouseX, Main.mouseY)))
            {
                Main.player[Main.myPlayer].mouseInterface = true;
            }
            Utils.DrawInvBG(sb, menuPosition.X + (float)(num / 2), menuPosition.Y + (float)(num * 5 / 2), menuSize.X / 2f - (float)num, menuSize.Y - (float)(num * 3), colors2[colorIndex]);
            Utils.DrawInvBG(sb, menuPosition.X + menuSize.X / 2f + (float)num, menuPosition.Y + (float)(num * 5 / 2), menuSize.X / 2f - (float)(num * 3 / 2), menuSize.Y - (float)(num * 3), colors2[colorIndex]);
            Utils.DrawBorderString(sb, "Mechmod Settings", menuPosition + menuSize * new Vector2(0.5f, 0f), Color.White, 1f, 0.5f, 0f, -1);
            float num2 = 0.7f;
            float num3 = 0.8f;
            float num4 = 0.01f;
            if (MechmodOptions.oldLeftHover != MechmodOptions.leftHover && MechmodOptions.leftHover != -1)
            {
                Main.PlaySound(12, -1, -1, 1);
            }
            if (MechmodOptions.oldRightHover != MechmodOptions.rightHover && MechmodOptions.rightHover != -1)
            {
                Main.PlaySound(12, -1, -1, 1);
            }
            if (flag && MechmodOptions.rightHover != -1 && !MechmodOptions.noSound)
            {
                Main.PlaySound(12, -1, -1, 1);
            }
			if (MechmodOptions.oldSubmenuHover != MechmodOptions.submenuHover && MechmodOptions.submenuHover != -1)
			{
				Main.PlaySound(12, -1, -1);
			}
			oldSubmenuHover = submenuHover;
			submenuHover = -1;

            MechmodOptions.oldLeftHover = MechmodOptions.leftHover;
            MechmodOptions.oldRightHover = MechmodOptions.rightHover;
            MechmodOptions.noSound = false;
            int num6 = 5; //amount of categories
            Vector2 anchor = new Vector2(menuPosition.X + menuSize.X / 4f, menuPosition.Y + (float)(num * 5 / 2));
            Vector2 offset = new Vector2(0f, menuSize.Y - (float)(num * 5)) / (float)(num6 + 1);
            for (int i = 0; i <= num6; i++)
            {
                if (MechmodOptions.leftHover == i || i == MechmodOptions.category)
                {
                    MechmodOptions.leftScale[i] += num4;
                }
                else
                {
                    MechmodOptions.leftScale[i] -= num4;
                }
                if (MechmodOptions.leftScale[i] < num2)
                {
                    MechmodOptions.leftScale[i] = num2;
                }
                if (MechmodOptions.leftScale[i] > num3)
                {
                    MechmodOptions.leftScale[i] = num3;
                }
            }
			
			Vector2 point;

            #region categories
            MechmodOptions.leftHover = -1;
            int num7 = MechmodOptions.category;
            if (MechmodOptions.DrawLeftSide(sb, "Cheats", 0, anchor, offset, MechmodOptions.leftScale, 0.7f, 0.8f, 0.01f))
            {
                MechmodOptions.leftHover = 0;
                if (flag)
                {
                    MechmodOptions.category = 0;
                    Main.PlaySound(10, -1, -1, 1);
                }
            }
            if (MechmodOptions.DrawLeftSide(sb, "Wiring", 1, anchor, offset, MechmodOptions.leftScale, 0.7f, 0.8f, 0.01f))
            {
                MechmodOptions.leftHover = 1;
                if (flag)
                {
                    MechmodOptions.category = 1;
                    Main.PlaySound(10, -1, -1, 1);
                }
            }
            if (MechmodOptions.DrawLeftSide(sb, "Ghosts", 2, anchor, offset, MechmodOptions.leftScale, 0.7f, 0.8f, 0.01f))
            {
                MechmodOptions.leftHover = 2;
                if (flag)
                {
                    MechmodOptions.category = 2;
                    Main.PlaySound(10, -1, -1, 1);
                }
            }
			if (MechmodOptions.DrawLeftSide(sb, "Hotbar", 3, anchor, offset, MechmodOptions.leftScale, 0.7f, 0.8f, 0.01f))
            {
                MechmodOptions.leftHover = 3;
                if (flag)
                {
                    MechmodOptions.category = 3;
                    Main.PlaySound(10, -1, -1, 1);
                }
            }

            //category to reset as well?
            //Category to reset as well.
			if (MechmodOptions.DrawLeftSide(sb, "Other", 4, anchor, offset, MechmodOptions.leftScale, 0.7f, 0.8f, 0.01f))
            {
                MechmodOptions.leftHover = 4;
                if (flag)
                {
                    MechmodOptions.category = 4;
                    Main.PlaySound(10, -1, -1, 1);
                }
            }
            if (MechmodOptions.DrawLeftSide(sb, "Close menu", 5, anchor, offset, MechmodOptions.leftScale, 0.7f, 0.8f, 0.01f))
            {
                MechmodOptions.leftHover = 5;
                if (flag)
                {
                    MechmodOptions.Close();
                    Main.PlaySound(10, -1, -1, 1);
                }
            }

            if (num7 != MechmodOptions.category)
            {
                for (int j = 0; j < MechmodOptions.rightScale.Length; j++)
                {
                    MechmodOptions.rightScale[j] = 0f;
                }
            }
            int num8 = 0;
            switch (MechmodOptions.category)
            {
                case -1: //Credits/about
                    num8 = 1;
                    num2 = 1f;
                    num3 = 1.001f;//not rly needed
                    num4 = 0.001f;
                    break;
                case 0://Cheats
                    num8 = 6;           //Number of items!
                    num2 = 1f;          //scaling stuff apparently, leave as is
                    num3 = 1.001f;      //scaling stuff apparently
                    num4 = 0.001f;      //scaling stuff apparently
                    break;
                case 1://Wiring
                    num8 = 11;
                    num2 = 1.0f;
                    num3 = 1.001f;
                    num4 = 0.001f;
                    break;
                case 2://ghosts
                    num8 = 7;
                    num2 = 1f;
                    num3 = 1.001f;
                    num4 = 0.001f;
                    break;
				case 3://hotbar
					num8 = 6;
					num2 = 1f;
					num3 = 1.001f;
                    num4 = 0.001f;
                    break;
                case 4:
                    num8 = Main.sunnyDay ? 7 : 6;
                    num2 = 1f;
                    num3 = 1.001f;
                    num4 = 0.001f;
                    break;
            }
            Vector2 anchor2 = new Vector2(menuPosition.X + menuSize.X * 3f / 4f, menuPosition.Y + (float)(num * 5 / 2));
            Vector2 offset2 = new Vector2(0f, menuSize.Y - (float)(num * 3)) / (float)(num8 + 1); //offset is multiplied by i?

            for (int k = 0; k < 15; k++)
            {
                if (MechmodOptions.rightLock == k || (MechmodOptions.rightHover == k && MechmodOptions.rightLock == -1))
                {
                    MechmodOptions.rightScale[k] += num4;
                }
                else
                {
                    MechmodOptions.rightScale[k] -= num4;
                }
                if (MechmodOptions.rightScale[k] < num2)
                {
                    MechmodOptions.rightScale[k] = num2;
                }
                if (MechmodOptions.rightScale[k] > num3)
                {
                    MechmodOptions.rightScale[k] = num3;
                }
            }
            MechmodOptions.inBar = false;
            MechmodOptions.rightHover = -1;
            if (!Main.mouseLeft)
            {
                MechmodOptions.rightLock = -1;
				submenuBarLock = -1;
            }
            if (MechmodOptions.rightLock == -1)
            {
                MechmodOptions.notBar = false;
            }

            #endregion
            #region About
            try
            {
                if (MechmodOptions.category == -1)
                {
                    //draw about screen

                    Color mouseTextColor = new Color(Main.mouseTextColor, Main.mouseTextColor, Main.mouseTextColor);
                    Color fivethreeColor = Color.Firebrick.MultiplyRGB(mouseTextColor);
                    Color stefnotchColor = Color.AliceBlue.MultiplyRGB(mouseTextColor); //Well, there is no color whose name starts with Bob..
                    mouseTextColor = new Color(Main.mouseTextColor / 3 + 100, Main.mouseTextColor / 3 + 100, Main.mouseTextColor / 3 + 100);


                    // ;)
                    string[] data =
                    {
                    "10001011100110100101000100111001110",
                    "11011010001000100101101101000101001",
                    "10101011101000111101010101000101001",
                    "10001010001000100101000101000101001",
                    "10001011100110100101000100111001110",
                };
                    int i = 0;
                    int j = 0;
                    int xmult = 8;
                    int ymult = 10;
                    List<Vector2> coords = new List<Vector2>();
                    Texture2D party = Main.itemTexture[1344];
                    foreach (string s in data)
                    {
                        foreach (char c in s)
                        {
                            if (c == '1')
                                coords.Add(new Vector2(i * xmult, j * ymult));
                            i++;
                        }
                        j++;
                        i = 0;
                    }
                    Vector2 partyOrigin = new Vector2(party.Width / 2, party.Height / 2);
                    Vector2 basePosition = new Vector2(anchor2.X - 140, anchor2.Y + 8) + partyOrigin;
                    float scale = 0.8f;
                    foreach (Vector2 coordinate in coords)
                    {
                        sb.Draw(party, coordinate + basePosition, null, Color.White, cogRotation, partyOrigin, scale, SpriteEffects.None, 0f);
                    }
                    cogRotation += .01f;





                    Vector2 by = Main.fontMouseText.MeasureString("by");
                    Vector2 stefnotch = Main.fontMouseText.MeasureString("stefnotch");
                    Vector2 and = Main.fontMouseText.MeasureString("and");
                    Vector2 fivethree = Main.fontMouseText.MeasureString("five--three");
                    float smallScale = 1.2f;
                    float largeSale = 1.8f;
                    float padding = 6f;
                    Vector2 pos = new Vector2(anchor2.X, anchor2.Y + 50 * 1.5f + padding * 4);
                    Utils.DrawBorderStringFourWay(sb, Main.fontMouseText, "by", pos.X, pos.Y, Color.White.MultiplyRGB(mouseTextColor), Color.Black, by / 2, smallScale);
                    pos.Y += (by.Y * smallScale) + padding;
                    Utils.DrawBorderStringFourWay(sb, Main.fontMouseText, "stefnotch", pos.X, pos.Y, stefnotchColor, Color.CadetBlue, stefnotch / 2, largeSale);
                    pos.Y += (stefnotch.Y);
                    Utils.DrawBorderStringFourWay(sb, Main.fontMouseText, "and", pos.X, pos.Y, Color.White.MultiplyRGB(mouseTextColor), Color.Black, and / 2, smallScale);
                    pos.Y += (and.Y * smallScale) + padding;
                    Utils.DrawBorderStringFourWay(sb, Main.fontMouseText, "five--three", pos.X, pos.Y, fivethreeColor, Color.Goldenrod, fivethree / 2, largeSale); //I dont know what these two colors are but they sound nice
                    pos.Y += (fivethree.Y);
                    //Utils.DrawBorderStringFourWay(sb, Main.fontCombatText[1], "Mechmod", anchor2.X, anchor2.Y + 40, Color.Gold, Color.RosyBrown, Main.fontCombatText[1].MeasureString("Mechmod") / 2);
                    pos.Y += 30;
                    /*

					float bottom = (menuPosition.Y + 50) + (menuSize.Y - 60);
					int rwidth = 220;
					int rheight = 110;
					Rectangle box = new Rectangle((int)anchor2.X - rwidth/2, (int)bottom - 20 - rheight, rwidth, rheight);
					int maxTransition = 100;
					if (easter && (transitionStage <= maxTransition))
					{
						transitionStage++;
					}

					Texture2D cogTexture = Main.itemTexture[1344];
					Texture2D redWrenchTexture = Main.itemTexture[509];
					Texture2D blueWrenchTexture = Main.itemTexture[850];
					Vector2 wrenchOrigin = redWrenchTexture.Bounds.Size();
					Vector2 cogOrigin = cogTexture.Bounds.Size()/2f;

					int cogAlpha = 255;
					float redWrenchRotation = -0.751f; //(tani(28/30)
					float blueWrenchRotation = -0.751f;
					float wrenchScale = 1f;
					if (transitionStage != 101)
					{
						cogPosition = box.Center.ToVector2();// i cry evrtime .....                  new Vector2(anchor2.X - ((92 * 1.5f) / 2) + 126, anchor2.Y + 4 + 31);
						cogAlpha = (int)(transitionStage * 2.55f);

						float redWrenchStartingRotation = -0.82f;
						float blueWrenchStartingRotation = -0.66f;
						float wrenchDesiredRotation = -0.751f;
						Vector2 redWrenchStartingPosition = new Vector2(pos.X - 10, pos.Y);
						Vector2 blueWrenchStartingPosition = new Vector2(pos.X + 10, pos.Y);

						Vector2 redWrenchDesiredPosition = new Vector2(box.X - 4, box.Center.Y + 20.52f);
						Vector2 blueWrenchDesiredPosition = new Vector2(box.Right + 4, box.Center.Y - 20.52f);

						wrenchScale = 1.5f - (.05f * transitionStage);

						redWrenchPos = (((redWrenchDesiredPosition - redWrenchStartingPosition) / maxTransition) * transitionStage) + redWrenchStartingPosition;
						blueWrenchPos = (((blueWrenchDesiredPosition - blueWrenchStartingPosition) / maxTransition) * transitionStage) + blueWrenchStartingPosition;
						redWrenchRotation = (((wrenchDesiredRotation - redWrenchStartingRotation) / maxTransition) * transitionStage) + redWrenchStartingRotation;
						blueWrenchRotation = (((wrenchDesiredRotation - blueWrenchStartingRotation) / maxTransition) * transitionStage) + blueWrenchStartingRotation;

						sb.Draw(Main.magicPixel, new Rectangle((int)redWrenchDesiredPosition.X - 2, (int)redWrenchDesiredPosition.Y - 2, 4, 4), Color.HotPink);
						sb.Draw(Main.magicPixel, new Rectangle((int)blueWrenchDesiredPosition.X - 2, (int)blueWrenchDesiredPosition.Y - 2, 4, 4), Color.HotPink);
						sb.Draw(Main.magicPixel, new Rectangle((int)redWrenchStartingPosition.X - 2, (int)redWrenchStartingPosition.Y - 2, 4, 4), Color.ForestGreen);
						sb.Draw(Main.magicPixel, new Rectangle((int)blueWrenchStartingPosition.X - 2, (int)blueWrenchStartingPosition.Y - 2, 4, 4), Color.ForestGreen);

						cogVelocity = new Vector2(2);
					}



					else {
						if (Main.keyState.IsKeyDown(Keys.Up))
						{
							redWrenchPos.Y--;
						}
						if (Main.keyState.IsKeyDown(Keys.Down))
						{
							redWrenchPos.Y++;
						}

						if (redWrenchPos.Y - 21 < box.Top)
						{ redWrenchPos.Y = box.Top + 21; }
						if (redWrenchPos.Y + 21 > box.Bottom)
						{ redWrenchPos.Y = box.Bottom - 21; }



						if (cogPosition.Y < box.Top || cogPosition.Y > box.Bottom)
							cogVelocity.Y = -cogVelocity.Y;
						if (cogPosition.Y < box.Left)
						{
								
						}
						if(cogPosition.X > box.Right)
						{

						}

						Rectangle redRectangle = new Rectangle((int)redWrenchPos.X + 16, (int)redWrenchPos.Y - 21, 6, 42);
						Rectangle blueRectangle = new Rectangle((int)blueWrenchPos.X + 20, (int)blueWrenchPos.Y - 21, 6, 42);
						drawRectangle(redRectangle, sb);
						drawRectangle(blueRectangle, sb);

						if(redRectangle.Contains(cogPosition.ToPoint()))
                        {
							cogVelocity.X = 2;
						}
						if(blueRectangle.Contains(cogPosition.ToPoint()))
                        {
							cogVelocity.X = -2;
						}
						cogPosition += cogVelocity;
					}



					
					sb.Draw(redWrenchTexture, redWrenchPos, null, Color.White, redWrenchRotation, default(Vector2), wrenchScale, 0, 0);
					sb.Draw(blueWrenchTexture, blueWrenchPos, null, Color.White, blueWrenchRotation, default(Vector2), wrenchScale, 0, 0);
					sb.Draw(cogTexture, cogPosition, null, new Color(cogAlpha, cogAlpha, cogAlpha, cogAlpha), cogRotation*10, cogOrigin, 1f, 0, 0f);

					drawRectangle(box, sb);


					if (flag && box.Contains(Main.mouseX, Main.mouseY))
					{
						easter = true;
						cogPosition = box.Center.ToVector2();
						cogVelocity = new Vector2(2);
						Main.sunnyDay = true;
					}


					//get to 7 points
					//on win: draw sunglasses on all npc's
					//armor_head_12
					*/
                }
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString());
            }
            #endregion
            #region Cheats
            if (MechmodOptions.category == 0)
            {
                int num9 = 0;
                //anchor2.X += 70f;//This adjusts the position for slider bars,
                //We don't want to do this.
                if (MechmodOptions.DrawRightSide(sb, Main.infiniteWire ? "Infinite wire enabled" : "Infinite wire disabled", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.infiniteWire = !Main.infiniteWire;
                    }
                    mouseText = (Main.infiniteWire ? "Wrenches will not require or consume any wire." : "Wrenches will use wire from your inventory.") + "\nWhen enabled, wire cutters will not drop any wire.";
                }
                num9++;  //Do this between elements
                if (MechmodOptions.DrawRightSide(sb, Main.infiniteActuators ? "Infinite actuators enabled" : "Infinite actuators disabled", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.infiniteActuators = !Main.infiniteActuators;
                    }
                    mouseText = (Main.infiniteActuators ? "Actuators will not be consumed when placed." : "Actuators will be consumed when placecd.") + "\nWhen enabled, wire cutters will not drop any actuators.";
                }
                num9++;
                if (MechmodOptions.DrawRightSide(sb, Main.godmode ? "Godmode enabled" : "Godmode disabled", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.godmode = !Main.godmode;
                    }
					mouseText = "When enabled, you will walk straight through enemies without being hit, have infinite health and mana";
				}
				//                      righthand side of the menu - width of the texture - a bit more padding,   yposition of text middle - half textureheight
				point = new Vector2(menuPosition.X + menuSize.X - (num * 3f / 2f) - cogSourceRect.Width - 2, anchor2.Y + offset2.Y*(1+num9) + Main.fontMouseText.LineSpacing/2f - cogSourceRect.Height*1.5f).Floor();
				//draw triangle
				Color color = Color.White;
				Rectangle area = new Rectangle((int)point.X - buttonLeniency, (int)point.Y - buttonLeniency, cogSourceRect.Width + buttonLeniency * 2, cogSourceRect.Height + buttonLeniency * 2);
				drawRectangle(area, sb);
                if (area.Contains(new Point(Main.mouseX, Main.mouseY)))
				{
					color = Color.Gold;
					if (flag)
					{
						currentSubmenu = Submenu.GODMODE;
						submenuLock = true;
						submenuPos = point - new Vector2(0, cogSourceRect.Height/2);
					}
                }
				sb.Draw(UITextures, point, cogSourceRect, color, 0f, Vector2.Zero, 1f, 0, 0);
                num9++;
				if (MechmodOptions.DrawRightSide(sb, Main.infiniteHealth ? "Infinite health enabled" : "Infinite health disabled", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
				{
					MechmodOptions.rightHover = num9;
					if (flag)
					{
						Main.infiniteHealth = !Main.infiniteHealth;
					}
					mouseText = "When enabled, you will not take any damage.";
				}
				num9++;/*
				if (MechmodOptions.DrawRightSide(sb, Main.infiniteMana ? "Infinite mana enabled" : "Infinite mana disabled", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
				{
					MechmodOptions.rightHover = num9;
					if (flag)
					{
						Main.infiniteMana = !Main.infiniteMana;
					}
					mouseText = "When enabled, no mana will be used.";
				}
				num9++;*/
                if (MechmodOptions.DrawRightSide(sb, !Main.placingLimits ? "No placing limits" : "Normal placing limits", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.placingLimits = !Main.placingLimits;
                    }
                    mouseText = "When enabled, you can place blocks anywhere.";
                }
                num9++;
                anchor2.X -= 70f;
                if (MechmodOptions.DrawRightSide(sb, string.Concat(new object[]
                {
                    "UFO speed:",
                    " ",
                    Math.Round((double)(Main.UFOSpeed * 100f)),
                    "%"
                }), num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), new Color(255, 128, 128)))
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.noSound = true;
                    MechmodOptions.rightHover = num9;
                }
                MechmodOptions.valuePosition.X = menuPosition.X + menuSize.X - (float)(num / 2) - 20f;
                MechmodOptions.valuePosition.Y = MechmodOptions.valuePosition.Y - 3f;
                float ufospeed = MechmodOptions.DrawValueBar(sb, 0.75f, Main.UFOSpeed, 5f);
                if ((MechmodOptions.inBar || MechmodOptions.rightLock == num9) && !MechmodOptions.notBar)
                {
                    MechmodOptions.rightHover = num9;
                    if (Main.mouseLeft && MechmodOptions.rightLock == num9)
                    {
                        Main.UFOSpeed = ufospeed;
                    }
                    mouseText = "Modifies the speed of the alien UFO mount.";
                }
                if ((float)Main.mouseX > menuPosition.X + menuSize.X * 2f / 3f + (float)num && (float)Main.mouseX < MechmodOptions.valuePosition.X + 3.75f && (float)Main.mouseY > MechmodOptions.valuePosition.Y - 10f && (float)Main.mouseY <= MechmodOptions.valuePosition.Y + 10f)
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.rightHover = num9;
                }
                anchor2.X += 70f;
                num9++;
                if (MechmodOptions.DrawRightSide(sb, "Reset UFO speed", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), Color.LightGray))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.UFOSpeed = 1f;
                    }
                }
                num9++;
            }
            #endregion
            #region Wiring
            if (MechmodOptions.category == 1)
            {
                int num10 = 0;
                if (MechmodOptions.DrawRightSide(sb, Main.brightRedWire ? "Red wire unaffected by light" : "Red wire affected by light", num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num10;
                    if (flag)
                    {
                        Main.brightRedWire = !Main.brightRedWire;
                    }
                    mouseText = "This setting dictates whether red wire will\nshow regardless of light level.";
                }
                num10++;
                if (MechmodOptions.DrawRightSide(sb, Main.brightBlueWire ? "Blue wire unaffected by light" : "Blue wire affected by light", num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num10;
                    if (flag)
                    {
                        Main.brightBlueWire = !Main.brightBlueWire;
                    }
                    mouseText = "This setting dictates whether blue wire will\nshow regardless of light level.";
                }
                num10++;
                if (MechmodOptions.DrawRightSide(sb, Main.brightGreenWire ? "Green wire unaffected by light" : "Green wire affected by light", num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num10;
                    if (flag)
                    {
                        Main.brightGreenWire = !Main.brightGreenWire; //todo Main.instance.MouseText("This dictates whether this color of wire will ignore lighting.")
                    }
                    mouseText = "This setting dictates whether green wire will\nshow regardless of light level.";
                }
                num10++;
                if (MechmodOptions.DrawRightSide(sb, Main.brightActuators ? "Actuators unaffected by light" : "Actuators affected by light", num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num10;
                    if (flag)
                    {
                        Main.brightActuators = !Main.brightActuators;
                    }
                    mouseText = "This setting dictates whether actuators will\nshow regardless of light level.";
                }
                num10++; //add seperator?
                         //Add seperator.
                MechmodOptions.DrawRightSide(sb, "-Wire opacity-", num10, anchor2, offset2, MechmodOptions.rightScale[num10], 0.9f, default(Color));
                num10++;

                anchor2.X -= 70f;
                if (MechmodOptions.DrawRightSide(sb, string.Concat(new object[]
                {
                    "Red:",
                    " ",
                    Math.Round((double)(Main.redWireBrightness * 100f)),
                    "%"
                }), num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), new Color(255, 128, 128)))
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.noSound = true;
                    MechmodOptions.rightHover = num10;
                }
                MechmodOptions.valuePosition.X = menuPosition.X + menuSize.X - (float)(num / 2) - 20f;
                MechmodOptions.valuePosition.Y = MechmodOptions.valuePosition.Y - 3f;
                float redBrightness = MechmodOptions.DrawValueBar(sb, 0.75f, Main.redWireBrightness);
                if ((MechmodOptions.inBar || MechmodOptions.rightLock == num10) && !MechmodOptions.notBar)
                {
                    MechmodOptions.rightHover = num10;
                    if (Main.mouseLeft && MechmodOptions.rightLock == num10)
                    {
                        Main.redWireBrightness = redBrightness;
                    }
                    mouseText = "The opactiy of red wire.";
                }
                if ((float)Main.mouseX > menuPosition.X + menuSize.X * 2f / 3f + (float)num && (float)Main.mouseX < MechmodOptions.valuePosition.X + 3.75f && (float)Main.mouseY > MechmodOptions.valuePosition.Y - 10f && (float)Main.mouseY <= MechmodOptions.valuePosition.Y + 10f)
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.rightHover = num10;
                }
                num10++;
                if (MechmodOptions.DrawRightSide(sb, string.Concat(new object[]
                {
                    "Blue:",
                    " ",
                    Math.Round((double)(Main.blueWireBrightness * 100f)),
                    "%"
                }), num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), new Color(128, 128, 255)))
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.noSound = true;
                    MechmodOptions.rightHover = num10;
                }
                MechmodOptions.valuePosition.X = menuPosition.X + menuSize.X - (float)(num / 2) - 20f;
                MechmodOptions.valuePosition.Y = MechmodOptions.valuePosition.Y - 3f;
                float blueBrightness = MechmodOptions.DrawValueBar(sb, 0.75f, Main.blueWireBrightness);
                if ((MechmodOptions.inBar || MechmodOptions.rightLock == num10) && !MechmodOptions.notBar)
                {
                    MechmodOptions.rightHover = num10;
                    if (Main.mouseLeft && MechmodOptions.rightLock == num10)
                    {
                        Main.blueWireBrightness = blueBrightness;
                    }
                    mouseText = "The opacity of blue wire.";
                }
                if ((float)Main.mouseX > menuPosition.X + menuSize.X * 2f / 3f + (float)num && (float)Main.mouseX < MechmodOptions.valuePosition.X + 3.75f && (float)Main.mouseY > MechmodOptions.valuePosition.Y - 10f && (float)Main.mouseY <= MechmodOptions.valuePosition.Y + 10f)
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.rightHover = num10;
                }
                num10++;
                if (MechmodOptions.DrawRightSide(sb, string.Concat(new object[]
                {
                    "Green:",
                    " ",
                    Math.Round((double)(Main.greenWireBrightness * 100f)),
                    "%"
                }), num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), new Color(128, 255, 128)))
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.noSound = true;
                    MechmodOptions.rightHover = num10;
                }
                MechmodOptions.valuePosition.X = menuPosition.X + menuSize.X - (float)(num / 2) - 20f;
                MechmodOptions.valuePosition.Y = MechmodOptions.valuePosition.Y - 3f;
                float greenBrightness = MechmodOptions.DrawValueBar(sb, 0.75f, Main.greenWireBrightness);
                if ((MechmodOptions.inBar || MechmodOptions.rightLock == num10) && !MechmodOptions.notBar)
                {
                    MechmodOptions.rightHover = num10;
                    if (Main.mouseLeft && MechmodOptions.rightLock == num10)
                    {
                        Main.greenWireBrightness = greenBrightness;
                    }
                    mouseText = "The opacity of green wire.";
                }
                if ((float)Main.mouseX > menuPosition.X + menuSize.X * 2f / 3f + (float)num && (float)Main.mouseX < MechmodOptions.valuePosition.X + 3.75f && (float)Main.mouseY > MechmodOptions.valuePosition.Y - 10f && (float)Main.mouseY <= MechmodOptions.valuePosition.Y + 10f)
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.rightHover = num10;
                }
                num10++;
                if (MechmodOptions.DrawRightSide(sb, string.Concat(new object[]
                {
                    "Actuators:",
                    " ",
                    Math.Round((double)(Main.actuatorBrightness * 100f)),
                    "%"
                }), num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), new Color(255, 255, 128)))
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.noSound = true;
                    MechmodOptions.rightHover = num10;
                }
				// This is really weird.
                MechmodOptions.valuePosition.X = menuPosition.X + menuSize.X - (float)(num / 2) - 20f;
                MechmodOptions.valuePosition.Y = MechmodOptions.valuePosition.Y - 3f;
                float actuatorBright = MechmodOptions.DrawValueBar(sb, 0.75f, Main.actuatorBrightness);
                if ((MechmodOptions.inBar || MechmodOptions.rightLock == num10) && !MechmodOptions.notBar)
                {
                    MechmodOptions.rightHover = num10;
                    if (Main.mouseLeft && MechmodOptions.rightLock == num10)
                    {
                        Main.actuatorBrightness = actuatorBright;
                    }
                    mouseText = "The brightness of actuators.";
                }
                if ((float)Main.mouseX > menuPosition.X + menuSize.X * 2f / 3f + (float)num && (float)Main.mouseX < MechmodOptions.valuePosition.X + 3.75f && (float)Main.mouseY > MechmodOptions.valuePosition.Y - 10f && (float)Main.mouseY <= MechmodOptions.valuePosition.Y + 10f)
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.rightHover = num10;
                }
                anchor2.X += 70f;
                num10++;

                if (MechmodOptions.DrawRightSide(sb, Main.alwaysShowWires ? "Always show wires" : "Conditionally show wires", num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num10;
                    if (flag)
                    {
                        Main.alwaysShowWires = !Main.alwaysShowWires;
                    }
					mouseText = Main.alwaysShowWires ? "Wires will show regardless of the currently selected item." : "Wires will only show when a mechanical item is selected.";
                }
                num10++;
                if (MechmodOptions.DrawRightSide(sb, Main.wrenchHighlight ? "Wrenches highlight wire" : "Wrenches don't highlight wire", num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num10;
                    if (flag)
                    {
                        Main.wrenchHighlight = !Main.wrenchHighlight;
                    }
                    mouseText = "If enabled, chains of wire under the mouse will be highlighted if the player is holding a wrench of the respective color.\nThis feature is otherwise available with the Mechanical Ruler, bought from the Mechanic.";
                }
                num10++;
                /*
				if (MechmodOptions.DrawRightSide(sb, Lang.menu[59 + Main.qaStyle], num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
				{
					MechmodOptions.rightHover = num10;
					if (flag)
					{
						Main.qaStyle++;
						if (Main.qaStyle > 3)
						{
							Main.qaStyle = 0;
						}
					}
				}
				num10++;
				if (MechmodOptions.DrawRightSide(sb, Main.owBack ? Lang.menu[100] : Lang.menu[101], num10, anchor2, offset2, MechmodOptions.rightScale[num10], (MechmodOptions.rightScale[num10] - num2) / (num3 - num2), default(Color)))
				{
					MechmodOptions.rightHover = num10;
					if (flag)
					{
						Main.owBack = !Main.owBack;
					}
				}
				num10++;*/
            }
            #endregion
            #region Ghosts
            if (MechmodOptions.category == 2)
            {
                int num9 = 0; //lol we can leave this here

                //anchor2.X += 70f;//This adjusts the position for slider bars,
                //We don't want to do this.
                if (MechmodOptions.DrawRightSide(sb, Main.showDummyGhosts ? "Dummy ghosts enabled" : "Dummy ghosts disabled", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.showDummyGhosts = !Main.showDummyGhosts;
                    }
                    mouseText = (Main.showDummyGhosts ? "Dummy 'ghosts' will be shown." : "Dummy 'ghosts' are hidden.");
                }
                num9++;  //Do this between elements
                if (MechmodOptions.DrawRightSide(sb, Main.dummyGhostBright ? "Ghosts are unaffected by light" : "Ghosts are affected by light", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.dummyGhostBright = !Main.dummyGhostBright;
                    }
                    mouseText = Main.dummyGhostBright ? "Dummy ghosts will be shown regardless of the light level in their area." : "Dummy ghosts will be darkened according to nearby light levels.";
                }
                num9++;
                if (MechmodOptions.DrawRightSide(sb, Main.dummyGhostOutline ? "Dummy ghosts outlined in red" : "Dummy ghosts not outlined", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.dummyGhostOutline = !Main.dummyGhostOutline;
                    }
                    mouseText = "When enabled, dummy ghosts will be shown with a thick red outline.";
                }
                num9++;
                if (MechmodOptions.DrawRightSide(sb, Main.alwaysSpawnDummies ? "Dummies spawn when activated" : "Dummies spawn on world load", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.alwaysSpawnDummies = !Main.alwaysSpawnDummies;
                    }
					mouseText = "In vanilla Terraria, dummies only spawn when a player enters a 100x100 block rectangle around them." + "\n" + "If this setting is enabled, they will spawn regardless of the player position."; 
                }

                num9++;
                if (MechmodOptions.DrawRightSide(sb, "Kill wayward ghosts", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), Color.LightGray))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        MechmodOptions.Close();
                        Main.playerInventory = false;
                        Main.NewText(Main.killDummies().ToString() + " rogues killed.", 64, 255, 64, true);
                    }
                    mouseText = "When dummies are moved with TEdit, the ghosts remain in the save file." + "\n" + " These rogue ghosts are unkillable, unmovable, and can not get hit by projectiles." + "\n" + "Click here to attempt to remove them.";
                }
                num9++;
                anchor2.X -= 70f;
                if (MechmodOptions.DrawRightSide(sb, string.Concat(new object[]
                {
                    "Brightness",
                    " ",
                    Math.Round((double)(Main.dummyGhostBrightness * 100f)),
                    "%"
                }), num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2), new Color(255, 128, 128)))
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.noSound = true;
                    MechmodOptions.rightHover = num9;
                }
                MechmodOptions.valuePosition.X = menuPosition.X + menuSize.X - (float)(num / 2) - 20f;
                MechmodOptions.valuePosition.Y = MechmodOptions.valuePosition.Y - 3f;
                float brightness = MechmodOptions.DrawValueBar(sb, 0.75f, Main.dummyGhostBrightness);
                if ((MechmodOptions.inBar || MechmodOptions.rightLock == num9) && !MechmodOptions.notBar)
                {
                    MechmodOptions.rightHover = num9;
                    if (Main.mouseLeft && MechmodOptions.rightLock == num9)
                    {
                        Main.dummyGhostBrightness = brightness;
                    }
                    mouseText = "The opacity of dummy ghosts.";
                }
                if ((float)Main.mouseX > menuPosition.X + menuSize.X * 2f / 3f + (float)num && (float)Main.mouseX < MechmodOptions.valuePosition.X + 3.75f && (float)Main.mouseY > MechmodOptions.valuePosition.Y - 10f && (float)Main.mouseY <= MechmodOptions.valuePosition.Y + 10f)
                {
                    if (MechmodOptions.rightLock == -1)
                    {
                        MechmodOptions.notBar = true;
                    }
                    MechmodOptions.rightHover = num9;
                }
                anchor2.X += 70f;
                num9++;
                if (MechmodOptions.DrawRightSide(sb, Main.showDummyID ? "Show dummy IDs" : "Hide dummy IDs", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.showDummyID = !Main.showDummyID;
                    }
					mouseText = "Show/hide the internal NPC IDs for each dummy.";
                }
                num9++;
                if (MechmodOptions.DrawRightSide(sb, Main.dummyFeet ? "Show ghost feet" : "Hide ghost feet", num9, anchor2, offset2, MechmodOptions.rightScale[num9], (MechmodOptions.rightScale[num9] - num2) / (num3 - num2)))
                {
                    MechmodOptions.rightHover = num9;
                    if (flag)
                    {
                        Main.dummyFeet = !Main.dummyFeet;
                    }
                }
                num9++;
            }
			
            #endregion
			#region Hotbar
            if (MechmodOptions.category == 3)
            {
                int num13 = 0;
				//anchor2.X -= 30f;
				if (MechmodOptions.DrawRightSide(sb, "Hotkeys", num13, anchor2, offset2, MechmodOptions.rightScale[num13], (MechmodOptions.rightScale[num13] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num13;
                    if (flag)
                    {
						currentSubmenu = Submenu.HOTKEYS;
						submenuLock = true;
						submenuPos = new Vector2(menuPosition.X + menuSize.X - (num * 3f / 2f) - cogSourceRect.Width - 2, anchor2.Y + offset2.Y * (1 + num13) + Main.fontMouseText.LineSpacing / 2f - cogSourceRect.Height * 1.5f) - new Vector2(0, cogSourceRect.Height / 2);
                    }
					mouseText = "The first 4 hotbar slots are mapped to hotkeys. Click to rebind them.";
                }
                num13++;
				if (MechmodOptions.DrawRightSide(sb, "Misc toggles", num13, anchor2, offset2, MechmodOptions.rightScale[num13], (MechmodOptions.rightScale[num13] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num13;
                    if (flag)
                    {
						drawingMiscMenu = true;
                    }
					mouseText = "Open a menu of miscellaneous toggles.";
                }
				num13++;
				if (MechmodOptions.DrawRightSide(sb, "Clear Hotbar", num13, anchor2, offset2, MechmodOptions.rightScale[num13], (MechmodOptions.rightScale[num13] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num13;
                    if (flag)
                    {
						HotbarButton.RemoveAll();
                    }
					mouseText = "Remove all buttons from the hotbar.";
                }
                num13++;
                }
			#endregion
			#region Other
			if (MechmodOptions.category == 4)
                    {
				int num17 = 0;

				if (MechmodOptions.DrawRightSide(sb, "Reset all Mechmod settings", num17, anchor2, offset2, MechmodOptions.rightScale[num17], (MechmodOptions.rightScale[num17] - num2) / (num3 - num2), default(Color)))
                {
					MechmodOptions.rightHover = num17;
                    if (flag)
                    {
						if (resetConfirm)
                    {
							ConfigHandler.configFileSetup(true);
							resetConfirm = false;
							return;
                }
						else
                    {
							resetConfirm = true;
							flag = false;
                }
							
                    }
					mouseText = "Once reset, settings are not recoverable." + "\n" + (resetConfirm ? "Click again to confirm." : "Are you sure?");
                }
				num17++;  //Do this between elements
				if (MechmodOptions.DrawRightSide(sb, Main.showMechmodSettingButton ? "Inventory button enabled" : "Inventory button hidden", num17, anchor2, offset2, MechmodOptions.rightScale[num17], (MechmodOptions.rightScale[num17] - num2) / (num3 - num2), default(Color)))
                {
					MechmodOptions.rightHover = num17;
                    if (flag)
                    {
						Main.showMechmodSettingButton = !Main.showMechmodSettingButton;
                    }
					mouseText = "Shows a (glitchy) button on the inventory menu." + "\n" + "The button will overlap the defence icon sometimes, so it is disabled by default";
                }
				num17++;
				if (MechmodOptions.DrawRightSide(sb, "Color: " + (colorIndex + 1).ToString(), num17, anchor2, offset2, MechmodOptions.rightScale[num17], (MechmodOptions.rightScale[num17] - num2) / (num3 - num2), default(Color)))
                {
					MechmodOptions.rightHover = num17;
                    if (flag)
                    {
						colorIndex++;
						if (colorIndex == colors.Length)
							colorIndex = 0;
                    }
					mouseText = "Changes the menu color. Default: 1";
                }
				num17++;
				if (MechmodOptions.DrawRightSide(sb, "Hotbar Color: " + (HotbarButton.hcolorIndex + 1).ToString(), num17, anchor2, offset2, MechmodOptions.rightScale[num17], (MechmodOptions.rightScale[num17] - num2) / (num3 - num2), default(Color)))
                {
					MechmodOptions.rightHover = num17;
                    if (flag)
                    {
						HotbarButton.hcolorIndex++;
						if (HotbarButton.hcolorIndex == colors.Length)
							HotbarButton.hcolorIndex = 0;
                    }
					mouseText = "Changes the hotbar color. Default: 1";
                }
				num17++;
				if (MechmodOptions.DrawRightSide(sb, !Main.clouds ? "No clouds" : "Clouds", num17, anchor2, offset2, MechmodOptions.rightScale[num17], (MechmodOptions.rightScale[num17] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num17;
                    if (flag)
                    {
                        Main.clouds = !Main.clouds;
                        if (!Main.clouds) //If there aren't any clouds
                {
                            for (int i = 0; i < 200; i++)
                    {
                                Main.cloud[i].active = false;
                    }
                }
                    }
                    mouseText = "Toggle all the clouds!";
                }
                num17++;
                if (MechmodOptions.DrawRightSide(sb, Main.TPParticles ? "Teleporter particles visible" : "Teleporter particles invisible", num17, anchor2, offset2, MechmodOptions.rightScale[num17], (MechmodOptions.rightScale[num17] - num2) / (num3 - num2), default(Color)))
                {
                    MechmodOptions.rightHover = num17;
                    if (flag)
                    {
                        Main.TPParticles = !Main.TPParticles;
                    }
                    mouseText = "This setting dictates the Teleporter particles visibility.";
                }
                num17++;
                if (Main.sunnyDay && MechmodOptions.DrawRightSide(sb, "Disable easter egg", num17, anchor2, offset2, MechmodOptions.rightScale[num17], (MechmodOptions.rightScale[num17] - num2) / (num3 - num2), default(Color)))
                {
					MechmodOptions.rightHover = num17;
                    if (flag)
                    {
						Main.sunnyDay = false;
                }
					mouseText = "Will need to be re-unlocked.";
                    }
				num17++;
                }
			#endregion
			#region Submenu Overlays
			DrawSubmenus(sb, ref currentSubmenu, ref mouseText);
			#endregion
			if (MechmodOptions.rightHover != -1 && MechmodOptions.rightLock == -1)
                    {
				MechmodOptions.rightLock = MechmodOptions.rightHover;
                }
			if (MechmodOptions.submenuHover != -1 && MechmodOptions.submenuBarLock == -1)
                {
				MechmodOptions.submenuBarLock = MechmodOptions.submenuHover;
                }
			if (resetConfirm && flag)
                    {
				resetConfirm = false;
                }
			END:
			Main.instance.GUIBarsDraw();
			Main.instance.DrawMouseOver();
			Vector2 value4 = Main.DrawThickCursor(false);
			sb.Draw(Main.cursorTextures[0], new Vector2((float)Main.mouseX, (float)Main.mouseY) + value4 + Vector2.One, null, new Color((int)((float)Main.cursorColor.R * 0.2f), (int)((float)Main.cursorColor.G * 0.2f), (int)((float)Main.cursorColor.B * 0.2f), (int)((float)Main.cursorColor.A * 0.5f)), 0f, default(Vector2), Main.cursorScale * 1.1f, SpriteEffects.None, 0f);
			sb.Draw(Main.cursorTextures[0], new Vector2((float)Main.mouseX, (float)Main.mouseY) + value4, null, Main.cursorColor, 0f, default(Vector2), Main.cursorScale, SpriteEffects.None, 0f);
			Main.mouseText = false;
			if(mouseText != "")
                    {
				//Gotta do it here, otherwise text in following elements draws over it ( looks nasty )
				Main.instance.MouseText(mouseText);
				Main.mouseText = true;
                    }
                }
		public static void DrawSubmenus(SpriteBatch sb, ref Submenu currentSubmenu, ref string mouseText, Vector2 submenuPos = default(Vector2), bool bottomCentered = false)
                {
			
			if (submenuPos == default(Vector2))
				submenuPos = MechmodOptions.submenuPos;

			bool flag = !submenuLock && Main.mouseLeft && Main.mouseLeftRelease;
			if (currentSubmenu != Submenu.NONE)
				mouseText = "";
			#region GODMODE
			if (currentSubmenu == Submenu.GODMODE && category == 0)
                    {

				//So we want to size the menu based on what it contains. So, we have to define the menu options before drawing the menu:
				Dictionary<string, Action> things = new Dictionary<string, Action>()
                {
					//I almost miss JS's anonymous functions. But this is just as cool!
					[Main.infiniteHealth ? "Infinite health enabled" : "Infinite health disabled"] = (Action)(() => Main.infiniteHealth = !Main.infiniteHealth),
					[Main.infiniteMana ? "Infinite mana enabled" : "Infinite mana disabled"] = (Action)(() => Main.infiniteHealth = !Main.infiniteHealth),
					["Button label goes here"] = (Action)(() => { System.Windows.Forms.MessageBox.Show("What you want the button do do goes here!"); })
				};
				//I just remembered Terraria does calculations according to the number of categories too. This would have been much easier if I just had an `int numLabels` at the top. Oh well, this is cool. It also makes it much easier to add more basic toggles ...
				int padding = 10;
				Rectangle draw = new Rectangle((int)(submenuPos.X - padding), (int)(submenuPos.Y - (things.Count * Main.fontMouseText.LineSpacing) / 2 - padding + cogSourceRect.Height / 2f), 250 + padding * 2, (things.Count * Main.fontMouseText.LineSpacing) + padding * 2);
				if (bottomCentered)
					draw.Y -= ((things.Count * Main.fontMouseText.LineSpacing) / 2) + padding;
				Utils.DrawInvBG(sb, draw, colors2[colorIndex] * (.8f / 0.685f));
				//										adjust the calculation for 0.8 alpha instead of 0.685
				int i = 0;
				foreach (KeyValuePair<string, Action> kvp in things)
                    {
					Vector2 positionish = new Vector2(draw.X + draw.Width / 2, draw.Y + padding + (Main.fontMouseText.LineSpacing * i));
					Vector2 textSize = Utils.DrawBorderString(sb, kvp.Key, positionish, oldSubmenuHover == i ? Color.Gold : Color.White, (oldSubmenuHover == i ? 1.2f : 1f), 0.5f, 0f);
					if (new Rectangle((int)(positionish.X - textSize.X / 2), (int)(positionish.Y), (int)textSize.X, (int)textSize.Y).Contains(new Point(Main.mouseX, Main.mouseY)))
                {
                    if (flag)
                    {
							Main.PlaySound(12, -1, -1);
							kvp.Value.Invoke();
                    }
						mouseText = "Enables this feature while Godmode is active.";
						submenuHover = i;
                }
					i++;
				}
				if (draw.Contains(new Point(Main.mouseX, Main.mouseY)))
                {
					Main.player[Main.myPlayer].mouseInterface = true;
				}
				else if (flag && !submenuLock)
                    {
					//close thing
					currentSubmenu = Submenu.NONE;
                    }
                }
#endregion
			#region HOTKEYS
			if (currentSubmenu == Submenu.HOTKEYS && category == 3)
                {
				Dictionary<string, Action> things = new Dictionary<string, Action>()
                    {
					["Hotkey 1: " + (setHotkey == 1 ? "_" : Main.hotkey1)] = (Action)(() => setHotkey = 0),
					["Hotkey 2: " + (setHotkey == 2 ? "_" : Main.hotkey2)] = (Action)(() => setHotkey = 1),
					["Hotkey 3: " + (setHotkey == 3 ? "_" : Main.hotkey3)] = (Action)(() => setHotkey = 2),
					["Hotkey 4: " + (setHotkey == 4 ? "_" : Main.hotkey4)] = (Action)(() => setHotkey = 3),
				};
				int padding = 12;
				Rectangle draw = new Rectangle((int)(submenuPos.X - padding), (int)(submenuPos.Y - (things.Count * Main.fontMouseText.LineSpacing) / 2 - padding + cogSourceRect.Height / 2f), 250 + padding * 2, (things.Count * Main.fontMouseText.LineSpacing) + padding * 2);
				if (bottomCentered)
					draw.Y -= ((things.Count * Main.fontMouseText.LineSpacing) / 2) + padding;
				Utils.DrawInvBG(sb, draw, colors2[colorIndex] * (.8f / .685f));
				//										adjust the calculation for 0.8 alpha instead of 0.685
				int i = 0;
				foreach (KeyValuePair<string, Action> kvp in things)
				{
					Vector2 positionish = new Vector2(draw.X + draw.Width / 2, draw.Y + padding + (Main.fontMouseText.LineSpacing * i));
					Vector2 textSize = Utils.DrawBorderString(sb, kvp.Key, positionish, (setHotkey == i ? Color.Gold : Color.White), (oldSubmenuHover == i ? 1.2f : 1f), 0.5f, 0f);
					if (new Rectangle((int)(positionish.X - textSize.X / 2), (int)(positionish.Y), (int)textSize.X, (int)textSize.Y).Contains(new Point(Main.mouseX, Main.mouseY)))
                {
                    if (flag)
                    {
							Main.PlaySound(12, -1, -1);
							kvp.Value.Invoke();
                    }
						submenuHover = i;
                }
					i++;
				}
				if (draw.Contains(new Point(Main.mouseX, Main.mouseY)))
                    {
					Main.player[Main.myPlayer].mouseInterface = true;
                    }
				else if (flag)
				{
					//close thing
					currentSubmenu = Submenu.NONE;
                }
				if (setHotkey >= 0)
                {
                    Main.blockInput = true;
                    Keys[] pressedKeys = Main.keyState.GetPressedKeys();
                    if (pressedKeys.Length > 0)
                    {
                        string text = string.Concat(pressedKeys[0]);
                        if (text != "None")
                        {
							if (setHotkey == 0)
                            {
								Main.hotkey1 = text;
                            }
							if (setHotkey == 1)
                            {
								Main.hotkey2 = text;
                            }
							if (setHotkey == 2)
                            {
								Main.hotkey3 = text;
                            }
							if (setHotkey == 3)
                            {
								Main.hotkey4 = text;
                            }
                            Main.blockKey = pressedKeys[0];
                            Main.blockInput = false;
							setHotkey = -1;
                        }
                    }
                }
            }
            #endregion
			#region WIREOPACITY
			if (currentSubmenu == Submenu.WIREOPACITY)
            {

				Dictionary<string, Action<float>> things = new Dictionary<string, Action<float>>()
                        {
					["Red wire"] = (Action<float>)(brightness => Main.redWireBrightness = brightness),
					["Blue wire"] = (Action<float>)(brightness => Main.blueWireBrightness = brightness),
					["Green wire"] = (Action<float>)(brightness => Main.greenWireBrightness = brightness),
					["Actuators"] = (Action<float>)(brightness => Main.actuatorBrightness = brightness)

				};
				int padding = 10;
				Rectangle draw = new Rectangle((int)(submenuPos.X - padding), (int)(submenuPos.Y - (things.Count * Main.fontMouseText.LineSpacing) / 2 - padding + cogSourceRect.Height / 2f), 250 + padding * 2, (things.Count * Main.fontMouseText.LineSpacing) + padding * 2);
				if (bottomCentered)
					draw.Y -= ((things.Count * Main.fontMouseText.LineSpacing) / 2) + padding;
                Utils.DrawInvBG(sb, draw, colors2[colorIndex] * (.8f / 0.685f));
				//										adjust the calculation for 0.8 alpha instead of 0.685
				int i = 0;
				Console.Clear();
				foreach (KeyValuePair<string, Action<float>> kvp in things)
				{
					//Console.WriteLine($"brightness: {Main.redWireBrightness.ToString().PadRight(6)}, notbar: {notBar}, inbar: {inBar}, hover: {submenuHover}, barLock{submenuBarLock}");


					Vector2 positionish = new Vector2(draw.X + draw.Width / 2 - 75f, draw.Y + padding + (Main.fontMouseText.LineSpacing * i));
					Vector2 textSize = Utils.DrawBorderString(sb, kvp.Key, positionish, oldSubmenuHover == i ? Color.Gold : Color.White, 1f, 0.5f, 0f);
					if (new Rectangle((int)(positionish.X - textSize.X / 2), (int)(positionish.Y), (int)textSize.X, (int)textSize.Y).Contains(new Point(Main.mouseX, Main.mouseY)))
                {
                    if (flag)
                    {
							Main.PlaySound(12, -1, -1);
                }
						
						if (MechmodOptions.submenuBarLock == -1)
                    {
							MechmodOptions.notBar = true;
                    }
						MechmodOptions.noSound = true;
						MechmodOptions.submenuHover = i;
						
						
                }
					MechmodOptions.valuePosition.X = draw.X + draw.Width - padding;
					MechmodOptions.valuePosition.Y = positionish.Y + textSize.Y * 0.5f;
					float value = i == 0 ? Main.redWireBrightness : (i == 1 ? Main.blueWireBrightness : (i == 2 ? Main.greenWireBrightness : Main.actuatorBrightness));
						//bit of a hack bc sliders
					float brightness = MechmodOptions.DrawValueBar(sb, 0.5f, value);
					if ((MechmodOptions.inBar || MechmodOptions.submenuBarLock == i) && !MechmodOptions.notBar)
                    {
						MechmodOptions.submenuHover = i;
						if (Main.mouseLeft && MechmodOptions.submenuBarLock == i)
                            {
							kvp.Value.Invoke(brightness);
                        }
                    }

					//Console.WriteLine($"brightness: {brightness.ToString().PadRight(6)}, notbar: {notBar}, inbar: {inBar}, hover: {submenuHover}, barLock{submenuBarLock}");
                    i++;
                }

				if (draw.Contains(new Point(Main.mouseX, Main.mouseY)))
                    {
					Main.player[Main.myPlayer].mouseInterface = true;
                    }
				else if (flag)
				{
					//close thing
					currentSubmenu = Submenu.NONE;
					HotbarButton.submenu = -1;
                }
            }
            #endregion

			if (submenuLock && !Main.mouseLeft)
				submenuLock = false;

		}
		public static void MouseOver()
		{
			if (!Main.isDrawingMechmodInterface)
			{
				return;
			}
			if (MechmodOptions._GUIHover.Contains(Main.MouseScreen.ToPoint()))
			{
				Main.mouseText = true;
			}
		}
		public static bool DrawLeftSide(SpriteBatch sb, string txt, int i, Vector2 anchor, Vector2 offset, float[] scales, float minscale = 0.7f, float maxscale = 0.8f, float scalespeed = 0.01f)
		{
			bool flag = i == MechmodOptions.category;
			Color color = Color.Lerp(Color.Gray, Color.White, (scales[i] - minscale) / (maxscale - minscale));
			if (flag)
			{
				color = Color.Gold;
			}
			Vector2 vector = Utils.DrawBorderStringBig(sb, txt, anchor + offset * (float)(1 + i), color, scales[i], 0.5f, 0.5f, -1);
			return new Rectangle((int)anchor.X - (int)vector.X / 2, (int)anchor.Y + (int)(offset.Y * (float)(1 + i)) - (int)vector.Y / 2, (int)vector.X, (int)vector.Y).Contains(new Point(Main.mouseX, Main.mouseY));
		}
		public static bool DrawRightSide(SpriteBatch sb, string txt, int i, Vector2 anchor, Vector2 offset, float scale, float colorScale, Color over = default(Color))
		{
			string key = "";
			bool gottaDrawTheHotbarButton = false;
			Color color = Color.Lerp(Color.Gray, Color.White, colorScale);
			if (over != default(Color))
			{
				color = over;
			}
			if(Main.keyState.IsKeyDown(Keys.LeftShift))
			{
				//if buttonMap has the key for this toggle, and if there is an associated button
				if (Hotbuttons.buttonMap[category].TryGetValue(i, out key) && Hotbuttons.buttons.ContainsKey(key))
					gottaDrawTheHotbarButton = true;
				//if shift is held but we can't draw this
				else
					color = new Color(70,70,70,40);
			}
			Vector2 value = Utils.DrawBorderString(sb, txt, anchor + offset * (float)(1 + i), color, scale, 0.5f, 0.5f, -1);
			Rectangle hitbox = new Rectangle((int)anchor.X - (int)value.X / 2, (int)anchor.Y + (int)(offset.Y * (float)(1 + i)) - (int)value.Y / 2, (int)value.X, (int)value.Y);
			if(gottaDrawTheHotbarButton)
			{
				Point center = hitbox.Center;
				Rectangle buttonBox = new Rectangle(center.X - 20, center.Y - 20, 40, 40);
				float colorMult = 0.85f;
				if (buttonBox.Contains(new Point(Main.mouseX, Main.mouseY)))
				{
					if (Main.mouseLeft && Main.mouseLeftRelease)
						Main.mouseHotbutton = key;
					colorMult = 1f;
				}

				sb.Draw(HotbarButton.hotbarBoxTexture, new Rectangle(center.X - 20, center.Y - 20, 40, 40), new Rectangle(0, 0, 40, 40), Color.Lerp(Color.Black, HotbarButton.colors[HotbarButton.hcolorIndex], colorMult), 0f, Vector2.Zero, 0, 0);

				HotbarTexture buttonTexture = Hotbuttons.buttons[key].texture;
				sb.Draw(buttonTexture.texture, hitbox.Center.ToVector2(), buttonTexture.sourceRect, Color.Lerp(Color.Black, Color.White, colorMult), 0f, new Vector2(buttonTexture.sourceRect.Width/2, buttonTexture.sourceRect.Height/2), 1f, 0, 0);
			}

			MechmodOptions.valuePosition = anchor + offset * (float)(1 + i) + value * new Vector2(0.5f, 0f);
			return !gottaDrawTheHotbarButton && hitbox.Contains(new Point(Main.mouseX, Main.mouseY));
		}
		public static bool DrawValue(SpriteBatch sb, string txt, int i, float scale, Color over = default(Color))
		{
			Color color = Color.Gray;
			Vector2 vector = Main.fontMouseText.MeasureString(txt) * scale;
			bool flag = new Rectangle((int)MechmodOptions.valuePosition.X, (int)MechmodOptions.valuePosition.Y - (int)vector.Y / 2, (int)vector.X, (int)vector.Y).Contains(new Point(Main.mouseX, Main.mouseY));
			if (flag)
			{
				color = Color.White;
			}
			if (over != default(Color))
			{
				color = over;
			}
			Utils.DrawBorderString(sb, txt, MechmodOptions.valuePosition, color, scale, 0f, 0.5f, -1);
			MechmodOptions.valuePosition.X = MechmodOptions.valuePosition.X + vector.X;
			return flag;
		}
		public static float DrawValueBar(SpriteBatch sb, float scale, float perc, float max = 1f)
		{
			Texture2D colorBarTexture = Main.colorBarTexture;
			Vector2 vector = new Vector2((float)colorBarTexture.Width, (float)colorBarTexture.Height) * scale;
			MechmodOptions.valuePosition.X = MechmodOptions.valuePosition.X - (float)((int)vector.X);
			Rectangle destinationRectangle = new Rectangle((int)MechmodOptions.valuePosition.X, (int)MechmodOptions.valuePosition.Y - (int)vector.Y / 2, (int)vector.X, (int)vector.Y);
			sb.Draw(colorBarTexture, destinationRectangle, Color.White);
			int num = 167;
			float num2 = (float)destinationRectangle.X + 5f * scale;
			float num3 = (float)destinationRectangle.Y + 4f * scale;
			for (float num4 = 0f; num4 < (float)num; num4 += 1f)
			{
				float amount = num4 / (float)num;
				sb.Draw(Main.colorBlipTexture, new Vector2(num2 + num4 * scale, num3), null, Color.Lerp(Color.Black, Color.White, amount), 0f, Vector2.Zero, scale, SpriteEffects.None, 0f);
			}
			sb.Draw(Main.colorSliderTexture, new Vector2(num2 + 167f * scale * perc/max, num3 + 4f * scale), null, Color.White, 0f, new Vector2(0.5f * (float)Main.colorSliderTexture.Width, 0.5f * (float)Main.colorSliderTexture.Height), scale, SpriteEffects.None, 0f);
			destinationRectangle.X = (int)num2;
			destinationRectangle.Y = (int)num3;
			bool flag = destinationRectangle.Contains(new Point(Main.mouseX, Main.mouseY));
			if (Main.mouseX >= destinationRectangle.X && Main.mouseX <= destinationRectangle.X + destinationRectangle.Width)
			{
				MechmodOptions.inBar = flag;
				return ((float)(Main.mouseX - destinationRectangle.X) / (float)destinationRectangle.Width) * max;
			}
			MechmodOptions.inBar = false;
			if (destinationRectangle.X >= Main.mouseX)
			{
				return 0f;
			}
			return max;
		}

		public static void SaveSettings()
		{
				//configOptions.Add("dummyGhostThickTexture", false);
				//configOptions.Add("dummyGhostBright", false);
				//configOptions.Add("showDummyGhosts", false);
				//configOptions.Add("slomoRate", 2);
				//configOptions.Add("yellowWireBright", false); //For later
				//configOptions.Add("greenWireBright", false);
				//configOptions.Add("brightRedWire", false);
				//configOptions.Add("brightBlueWire", false);
				//configOptions.Add("brightActuators", false);
				//configOptions.Add("redWireBrightness", 1);
				//configOptions.Add("blueWireBrightness", 1);
				//configOptions.Add("greenWireBrightness", 1);
				//configOptions.Add("actuatorBrightness", 1);
				//configOptions.Add("infiniteWire", false);
				//configOptions.Add("infiniteActuators", false);
				//configOptions.Add("godmode", false);
				//configOptions.Add("infiniteHealth", false);
				//configOptions.Add("infiniteMana", false); //aka enableLastPrisimFun
				//configOptions.Add("UFOSpeed", false); 


			//Copy that ^^^ (from ConfigHandler) into data.txt and run this vvv
				//with open("data.txt", "r") as file:
				//	lines = file.readlines()
				//s = ""
				//for line in lines:
				//	string = line.split("\"")[1]
				//s += "ConfigHandler.configOptions[\"" + string + "\"] = Main." + string + ";\r\n"
				//print(s)


			ConfigHandler.configOptions["slomoRate"] = Main.SLOMO_RATE; 
			ConfigHandler.configOptions["brightGreenWire"] = Main.brightGreenWire;
			ConfigHandler.configOptions["brightRedWire"] = Main.brightRedWire;
			ConfigHandler.configOptions["brightBlueWire"] = Main.brightBlueWire;
			ConfigHandler.configOptions["brightActuators"] = Main.brightActuators;
			ConfigHandler.configOptions["redWireBrightness"] = Main.redWireBrightness;
			ConfigHandler.configOptions["blueWireBrightness"] = Main.blueWireBrightness;
			ConfigHandler.configOptions["greenWireBrightness"] = Main.greenWireBrightness;
			ConfigHandler.configOptions["actuatorBrightness"] = Main.actuatorBrightness;
			ConfigHandler.configOptions["alwaysShowWires"] = Main.alwaysShowWires;
			ConfigHandler.configOptions["infiniteWire"] = Main.infiniteWire;
			ConfigHandler.configOptions["infiniteActuators"] = Main.infiniteActuators;
			ConfigHandler.configOptions["godmode"] = Main.godmode;
			/*ConfigHandler.configOptions["infiniteHealth"] = Main.infiniteHealth;
			ConfigHandler.configOptions["infiniteMana"] = Main.infiniteMana;*/
			ConfigHandler.configOptions["UFOSpeed"] = Main.UFOSpeed;
			ConfigHandler.configOptions["colorIndex"] = colorIndex;
			ConfigHandler.configOptions["showMechmodSettingsOption"] = Main.showMechmodSettingButton;
			ConfigHandler.configOptions["sunnyDay"] = Main.sunnyDay;

			ConfigHandler.configOptions["dummyGhostBright"] = Main.dummyGhostBright;
			ConfigHandler.configOptions["dummyGhostBrightness"] = Main.dummyGhostBrightness;
			ConfigHandler.configOptions["alwaysSpawnDummies"] = Main.alwaysSpawnDummies;
			ConfigHandler.configOptions["dummyGhostOutline"] = Main.dummyGhostOutline;
			ConfigHandler.configOptions["showDummyGhosts"] = Main.showDummyGhosts;

			ConfigHandler.configOptions["wrenchHighlight"] = Main.wrenchHighlight;

			ConfigHandler.configOptions["hotbarButtons"] = HotbarButton.GetSaveString();

			ConfigHandler.WriteConfig();

		}
		public static void LoadSettings()
		{
			Main.SLOMO_RATE = (int)ConfigHandler.getFloat("slomoRate");
			//Main.yellowWireBright = ConfigHandler.getBool("yellowWireBright");
			Main.brightGreenWire = ConfigHandler.getBool("brightGreenWire");
			Main.brightRedWire = ConfigHandler.getBool("brightRedWire");
			Main.brightBlueWire = ConfigHandler.getBool("brightBlueWire");
			Main.brightActuators = ConfigHandler.getBool("brightActuators");
			Main.redWireBrightness = ConfigHandler.getFloat("redWireBrightness");
			Main.blueWireBrightness = ConfigHandler.getFloat("blueWireBrightness");
			Main.greenWireBrightness = ConfigHandler.getFloat("greenWireBrightness");
			Main.actuatorBrightness = ConfigHandler.getFloat("actuatorBrightness");
			Main.alwaysShowWires = ConfigHandler.getBool("alwaysShowWires");
			Main.infiniteWire = ConfigHandler.getBool("infiniteWire");
			Main.infiniteActuators = ConfigHandler.getBool("infiniteActuators");
			Main.godmode = ConfigHandler.getBool("godmode");
			/*Main.infiniteHealth = ConfigHandler.getBool("infiniteHealth");
>>>>>>> 3820318... Hotbar update!
			Main.infiniteMana = ConfigHandler.getBool("infiniteMana");*/
            Main.UFOSpeed = ConfigHandler.getFloat("UFOSpeed");
            colorIndex = (int)ConfigHandler.getFloat("colorIndex");
            Main.showMechmodSettingButton = ConfigHandler.getBool("showMechmodSettingsOption");
            Main.sunnyDay = ConfigHandler.getBool("sunnyDay");
            Main.dummyGhostBright = ConfigHandler.getBool("dummyGhostBright");
            Main.dummyGhostBrightness = ConfigHandler.getFloat("dummyGhostBrightness");
            Main.alwaysSpawnDummies = ConfigHandler.getBool("alwaysSpawnDummies");
            Main.dummyGhostOutline = ConfigHandler.getBool("dummyGhostOutline");
            Main.showDummyGhosts = ConfigHandler.getBool("showDummyGhosts");

            Main.wrenchHighlight = ConfigHandler.getBool("wrenchHighlight");
        }
        public static void drawRectangle(Rectangle box, SpriteBatch sb)
        {
            sb.Draw(Main.magicPixel, new Rectangle(box.X, box.Y, box.Width, 1), Color.White);
            sb.Draw(Main.magicPixel, new Rectangle(box.X, box.Y, 1, box.Height), Color.White);
            sb.Draw(Main.magicPixel, new Rectangle(box.X + box.Width, box.Y, 1, box.Height), Color.White);
            sb.Draw(Main.magicPixel, new Rectangle(box.X, box.Y + box.Height, box.Width, 1), Color.White);
        }

		public static void DrawMiscMenu(SpriteBatch sb)
		{
			string mouseText = "";

			Vector2 boxSize = new Vector2(width, height);
			Vector2 screenSize = new Vector2(Main.screenWidth, Main.screenHeight);
			Vector2 boxPosition = screenSize / 2 - boxSize / 2;

			int padding = 20;
			Utils.DrawInvBG(sb, boxPosition.X - padding, boxPosition.Y - padding, boxSize.X + padding * 2, boxSize.Y + padding * 2, colors[colorIndex]);
			Utils.DrawBorderString(sb, "Miscellaneous Hotbar toggles", new Vector2(boxPosition.X + boxSize.X / 2, boxPosition.Y), Color.White, 1f, 0.5f, 0.5f);
			int buttonPadding = 2;
			int size = 40;
			int max = (int)boxSize.X/size;
			Rectangle draw = new Rectangle((int)boxPosition.X, (int)boxPosition.Y + padding, size, size);
            foreach (HBContainer cont in Hotbuttons.buttons.Values)
			{
				if (cont.implemented)
					continue;
				draw.X += size + buttonPadding;
				if(draw.X > max * size)
				{
					draw.X = 0;
					draw.Y += size + buttonPadding;
				}
				float mult = 1f;
				if (draw.Contains(new Point(Main.mouseX, Main.mouseY)))
				{
					mult = 0.8f;
					if(Main.mouseLeft && Main.mouseLeftRelease)
					{
						Main.mouseHotbutton = cont.name;
						//HotbarButton.Add(cont.texture, cont.action, cont.function);
					}
					mouseText = cont.name;
				}
				Utils.DrawInvBG(sb, draw, new Color(colors2[HotbarButton.hcolorIndex].ToVector3() * mult));
				sb.Draw(cont.texture.texture, draw.Center.ToVector2(), cont.texture.sourceRect, new Color(Color.White.ToVector3() * 0.7f), 0f, cont.texture.sourceRect.Size() / 2f, 1, 0, 0);
			}

			Vector2 textPos = new Vector2(boxPosition.X, boxPosition.Y + padding + boxSize.Y);
			Vector2 textSize = Main.fontDeathText.MeasureString("< Back") * .8f;
			Color c = Color.White;
			if (new Rectangle((int)textPos.X, (int)textPos.Y - (int)textSize.Y, (int)textSize.X, (int)textSize.Y).Contains(new Point(Main.mouseX, Main.mouseY)))
            {
				c = Color.Gold;
				if(Main.mouseLeft && Main.mouseLeftRelease)
				{
					MechmodOptions.drawingMiscMenu = false;
					MechmodOptions.Open();
					MechmodOptions.category = 3;
				}
				Main.blockMouse = true;
			}
			Utils.DrawBorderStringBig(sb, "< Back", textPos, c, .8f, 0, 1);

			if (new Rectangle((int)boxPosition.X - padding, (int)boxPosition.Y - padding, (int)boxSize.X + padding * 2, (int)boxSize.Y + padding * 2).Contains(Main.mouseX, Main.mouseY))
			{
				Main.blockMouse = true;
			} else if(Main.mouseLeft && Main.mouseLeftRelease && (Main.mouseHotbutton != "" && !HotbarButton.mouseHover))
			{
				MechmodOptions.Close();
			}
			if (mouseText != "")
			{
				Main.instance.MouseText(mouseText);
				Main.mouseText = true;
			}
		}

		
    }
}